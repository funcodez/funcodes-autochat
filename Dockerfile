FROM openjdk:18
WORKDIR /app
COPY target/funcodes-autochat-1.0.0.jar funcodes-autochat-1.0.0.jar
CMD ["java", "-Dconsole.width=120", "-jar", "funcodes-autochat-1.0.0.jar"]

# ------------------------------------------------------------------------------
# BUILD
# ------------------------------------------------------------------------------
# docker build -t "funcodes-autochat-1.0.0" .
# docker run -p 5161:5161 "funcodes-autochat-1.0.0"
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# BUILDX:
# ------------------------------------------------------------------------------
# ??? docker buildx install
# ??? docker buildx create --name local
# ??? docker buildx use local
# ??? docker buildx build --platform linux/amd64,linux/arm64 -t "funcodes-autochat-1.0.0:latest" .
# ??? docker run -p 5161:5161 "funcodes-autochat-1.0.0"
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# MAINTENANCE
# ------------------------------------------------------------------------------
# docker rmi -f $(docker images -f dangling=true -q)
# docker image rm -f <image-id>
# docker container ls
# docker container stop <container-id>
# ------------------------------------------------------------------------------
