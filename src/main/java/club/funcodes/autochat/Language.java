// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat;

import java.util.regex.Pattern;

import org.refcodes.runtime.SystemProperty;

/**
 * Enumeration for the supported languages (used in interactive mode).
 */
public enum Language {

	DEFAULT("english", "eliza.script", "you", new String[] { "exit", "bye", "quit" }),

	DE("german", "eliza-de.script", "du", new String[] { "exit", "bye", "quit", "servus", "tschüss", "tschau" }),

	EN("emglish", "eliza-en.script", "you", new String[] { "exit", "bye", "quit" });

	private String _scriptName;
	private String _you;
	private String _languageName;
	private String[] _exitKeyWords;

	private Language( String aLanguageName, String aScriptName, String youWord, String[] exitKeyWords ) {
		_languageName = aLanguageName;
		_scriptName = aScriptName;
		_you = youWord;
		_exitKeyWords = exitKeyWords;
	}

	/**
	 * Returns the name of the script for the according {@link Language}.
	 */
	public String getScriptName() {
		return _scriptName;
	}

	/**
	 * Language specific "you".
	 * 
	 * @return "you" in the according language.
	 */
	public String you() {
		return _you;
	}

	/**
	 * Returns the language's name.
	 * 
	 * @return The name of the language.
	 */
	public String getLanguageName() {
		return _languageName;
	}

	/**
	 * Returns the "exit" keywords.
	 * 
	 * @return The according array.
	 */
	public String[] getExitKeywords() {
		return _exitKeyWords;
	}

	/**
	 * Returns true if the passed argument matches (ignoring the case) one of
	 * the "exit" keywords. The argument is trimmed and all "!" and "." are
	 * removed before testing!
	 * 
	 * @param aMessage The text to be tested.
	 * 
	 * @return True if we have an exit keyword.
	 */
	public boolean isExitKeyword( String aMessage ) {
		if ( aMessage != null && aMessage.length() != 0 ) {
			aMessage = aMessage.trim();
			aMessage = aMessage.replaceAll( Pattern.quote( "!" ), "" ).replaceAll( Pattern.quote( "." ), "" );
			for ( String eKeyWord : _exitKeyWords ) {
				if ( eKeyWord.equalsIgnoreCase( aMessage ) ) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Tries to determine the system's language of the user running this
	 * application.
	 * 
	 * @return The according language or the {@link #DEFAULT} language if the
	 *         user's language is not supported..
	 */
	public static Language toUserLanguage() {
		String theUserLang = SystemProperty.USER_LANGUAGE.getValue();
		if ( theUserLang != null && theUserLang.length() != 0 ) {
			theUserLang = theUserLang.trim().toUpperCase();
		}
		for ( Language eLang : values() ) {
			if ( eLang.name().equalsIgnoreCase( theUserLang ) ) {
				return eLang;
			}
		}
		return DEFAULT;
	}
}
