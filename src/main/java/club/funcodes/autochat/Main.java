// /////////////////////////////////////////////////////////////////////////////
// CLUB.FUNCODES
// /////////////////////////////////////////////////////////////////////////////
// This code is written and provided by Siegfried Steiner, Munich, Germany.
// Feel free to use it as skeleton for your own applications. Make sure you have
// considered the license conditions of the included artifacts (pom.xml).
// -----------------------------------------------------------------------------
// The CLUB.FUNCODES artifacts used by this template are copyright (c) by
// Siegfried Steiner, Munich, Germany and licensed under the following
// (see "http://en.wikipedia.org/wiki/Multi-licensing") licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat;

import static org.refcodes.cli.CliSugar.*;

import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.Condition;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.NoneOperand;
import org.refcodes.cli.QuietFlag;
import org.refcodes.cli.StringOption;
import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.exception.Trap;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.Properties;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.rest.HttpRestServer;
import org.refcodes.rest.RestRequestEvent;
import org.refcodes.rest.RestfulHttpServer;
import org.refcodes.runtime.Terminal;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.HorizAlignTextMode;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusCode;

import club.funcodes.autochat.eliza.Eliza;
import club.funcodes.autochat.service.ClientStateElizaRequest;
import club.funcodes.autochat.service.ClientStateElizaResponse;
import club.funcodes.autochat.service.ElizaRequest;
import club.funcodes.autochat.service.ElizaService;
import club.funcodes.autochat.service.InMemoryElizaService;
import club.funcodes.autochat.service.ServerSessionElizaResponse;
import club.funcodes.autochat.teams.TeamsConversationRequest;
import club.funcodes.autochat.teams.TeamsConversationResponse;

/**
 * A minimum REFCODES.ORG enabled HTTP driven command line interface (CLI)
 * application. Get inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "autochat";
	private static final String TITLE = ">" + NAME.toUpperCase() + "!";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String DESCRIPTION = "ELIZA chatbot (by Joseph Weizenbaum) talking to you via RESTful services and an MS-Teams endpoint or on the console (see [https://www.metacodes.pro/manpages/autochat_manpage]).";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/autochat_manpage]";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final int DEFAULT_PORT = 8080;
	private static final String DEFAULT_BASE_PATH = "/autochat";
	private static final String PROMPT = "$>";
	private static final String ELIZA = "ELIZA";
	private static final String PORT_PROPERTY = "port";
	private static final String BASE_PATH_PROPERTY = "basePath";
	private static final String INTERACTIVE_PROPERTY = "interactive";
	private static final String LANGUAGE_PROPERTY = "language";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ElizaService _service;
	private RestfulHttpServer _server;
	private boolean _isQuiet;
	private int _port;
	private String _basePath;
	private Language _language;
	private boolean _isInteractive;

	// /////////////////////////////////////////////////////////////////////////
	// MAIN:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Command line invocation, immediately starts the server (via
	 * {@link #startServer()}). Stop via "Ctrl-C".
	 * 
	 * @param args The command line arguments.
	 * 
	 * @throws IOException thrown in case of I/O related problems when starting
	 *         up the application.
	 */
	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final NoneOperand theNoneArg = none( "Starts with the default configuration." );
		final StringOption theBasePathArg = stringOption( 'b', "base-path", BASE_PATH_PROPERTY, "The base path on which the RESTful server is serving HTTP requests (defaults to \"/\")." );
		final IntOption thePortArg = intOption( 'p', "port", PORT_PROPERTY, "The TCP port to be used for the HTTP service." );
		final EnumOption<Language> theLanguageArg = enumOption( 'l', "language", Language.class, LANGUAGE_PROPERTY, "Use the according language <" + VerboseTextBuilder.asString( Language.values() ) + "> for the interactive mode." );
		final ConfigOption theConfigArg = configOption();
		final Flag theInitFlag = initFlag( false );
		final Flag theInteractiveFlag = flag( 'i', "interactive", INTERACTIVE_PROPERTY, "Start in interactive mode, making ELIZA chat with you on the console." );
		final Flag theQuietFlag = quietFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();

		// @formatter:off
		final Condition theArgsSyntax = cases(
			xor( theNoneArg, any( thePortArg, theBasePathArg, theQuietFlag, theConfigArg, theDebugFlag ) ), 
			and( theInteractiveFlag, optional( theLanguageArg, theQuietFlag, theDebugFlag ) ), 
			and( theInitFlag, optional( theConfigArg, theQuietFlag ) ),
			xor( theHelpFlag, and( theSysInfoFlag, any( theQuietFlag ) ) ) 
		);
		final Example[] theExamples = examples(
			example( "Start server on another base path", theBasePathArg ), 
			example( "Start server on another port and base path", thePortArg, theBasePathArg ), 
			example( "Start server in quiet mode", theQuietFlag ), 
			example( "Start server using given configuration", theConfigArg ),
			example( "Start in interactive (repl) mode", theInteractiveFlag ), 
			example( "Start using language in interactive (repl) mode", theInteractiveFlag, theLanguageArg ),
			example( "Initialize default config file", theInitFlag ),
			example( "Initialize specific config file", theInitFlag, theConfigArg),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		try {
			new Main( theArgsProperties ).start();
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	/**
	 * Starts the autochat application depending on its state either in
	 * interactive mode or as a server.
	 * 
	 * @throws IOException thrown in case starting the server caused problems.
	 * 
	 */
	private void start() throws IOException {
		if ( _isInteractive ) {
			startRepl();
		}
		else {
			startServer();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates the server. Start by invoking {@link #startServer()}.
	 * 
	 * @param aProperties The startup properties of the application.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         initializing the server.
	 */
	public Main( Properties aProperties ) throws IOException {
		this( aProperties.getIntOr( PORT_PROPERTY, DEFAULT_PORT ), aProperties.getOr( BASE_PATH_PROPERTY, DEFAULT_BASE_PATH ), aProperties.getBooleanOr( INTERACTIVE_PROPERTY, false ), aProperties.getEnumOr( Language.class, LANGUAGE_PROPERTY, Language.toUserLanguage() ), aProperties.getBoolean( QuietFlag.ALIAS ) );
	}

	/**
	 * Instantiates the server. Start by invoking {@link #startServer()}.
	 *
	 * @param aPort The port to use.
	 * @param aBasePath The base path to listen to.
	 * @param isInteractive True in case no(!) server is to be started and
	 *        instead a console chat is initiated.
	 * @param aLanguage the language
	 * @param isQuiet Be more verbose when true.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         initializing the server.
	 */
	public Main( int aPort, String aBasePath, boolean isInteractive, Language aLanguage, boolean isQuiet ) throws IOException {
		_basePath = aBasePath;
		_port = aPort;
		_isInteractive = isInteractive;
		_language = aLanguage;
		_isQuiet = isQuiet;
		if ( !_isQuiet ) {
			LOGGER.info( "Starting application <" + NAME + "> ..." );
		}
		if ( !_isInteractive ) {
			if ( !_isQuiet ) {
				LOGGER.info( "Initializing server on port <" + _port + "> for base path \"" + _basePath + "\"..." );
			}
			_service = new InMemoryElizaService();
			_server = new HttpRestServer();
			onCreateConversationSession( _server, _basePath );
			onContinueConversationForSession( _server, _basePath );
			onCreateConversationState( _server, _basePath );
			onContinueConversationForState( _server, _basePath );
			onTeamsConversation( _server, _basePath );
			onHttpException( _server );
		}
	}

	/**
	 * Manually creates a {@link Main} instance, useful for programmatically
	 * invoking the read-eval-print-loop.
	 * 
	 * @param isVerbose Be more verbose when true.
	 * 
	 * @return The created server, start with {@link #startServer()}.
	 * 
	 * @throws IOException thrown in case I/O related problems occurred.
	 */
	public static Main createReadEvalPrintLoop( boolean isVerbose ) throws IOException {
		return new Main( -1, null, true, Language.EN, isVerbose );
	}

	/**
	 * Manually creates a {@link Main} instance, useful for programmatically
	 * invoking the read-eval-print-loop.
	 * 
	 * @param aLanguage The {@link Language} to use.
	 * @param isVerbose Be more verbose when true.
	 * 
	 * @return The created server, start with {@link #startServer()}.
	 * 
	 * @throws IOException thrown in case I/O related problems occurred.
	 */
	public static Main createReadEvalPrintLoop( Language aLanguage, boolean isVerbose ) throws IOException {
		return new Main( -1, null, true, aLanguage, isVerbose );
	}

	/**
	 * Manually creates a {@link Main} instance, useful for programmatically
	 * invoking the server.
	 * 
	 * @param aPort The port to use.
	 * @param aBasePath The base path to listen to.
	 * @param isVerbose Be more verbose when true.
	 * 
	 * @return The created server, start with {@link #startServer()}.
	 * 
	 * @throws IOException thrown in case I/O related problems occurred.
	 */
	public static Main createServer( int aPort, String aBasePath, boolean isVerbose ) throws IOException {
		return new Main( aPort, aBasePath, false, null, isVerbose );
	}

	/**
	 * Manually creates a {@link Main} instance, useful for programmatically
	 * invoking the server.
	 *
	 * @param aPort The port to use.
	 * @param isVerbose Be more verbose when true.
	 * 
	 * @return The created server, start with {@link #startServer()}.
	 * 
	 * @throws IOException thrown in case I/O related problems occurred.
	 */
	public static Main createServer( int aPort, boolean isVerbose ) throws IOException {
		return new Main( aPort, DEFAULT_BASE_PATH, false, null, isVerbose );
	}

	// /////////////////////////////////////////////////////////////////////////
	// REPL:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The read-eval-print-loop for chatting on the console.
	 */
	public void startRepl() {
		if ( !_isQuiet ) {
			LOGGER.info( "Initializing REPL with language <" + _language.getLanguageName().toUpperCase() + "> (\"" + _language.name() + "\")..." );
			LOGGER.info( "Exit by entering one of the following:" + VerboseTextBuilder.asString( _language.getExitKeywords() ) );
			LOGGER.printTail();
		}
		final Eliza theEliza = new Eliza( _language.getScriptName() );
		final Console theConsole = System.console();
		String you;
		String eliza;
		if ( !_isQuiet ) {
			final int max = _language.you().length() > ELIZA.length() ? _language.you().length() : ELIZA.length();
			you = HorizAlignTextBuilder.asAligned( _language.you().toUpperCase(), max, HorizAlignTextMode.RIGHT );
			eliza = HorizAlignTextBuilder.asAligned( ELIZA, max, HorizAlignTextMode.RIGHT );
			String prompt = " " + PROMPT + " ";
			if ( Terminal.isAnsiTerminalEnabled() ) {
				you = AnsiEscapeCode.ITALIC.toEscapeSequence() + you + AnsiEscapeCode.RESET.toEscapeSequence();
				eliza = AnsiEscapeCode.ITALIC.toEscapeSequence() + eliza + AnsiEscapeCode.RESET.toEscapeSequence();
				prompt = AnsiEscapeCode.FAINT.toEscapeSequence() + prompt + AnsiEscapeCode.RESET.toEscapeSequence();
			}
			you += prompt;
			eliza += prompt;
		}
		else {
			eliza = ELIZA + ": ";
			you = _language.you().toUpperCase() + ": ";
		}

		if ( theConsole == null ) {
			throw new IllegalStateException( "No console found for running in interactive mode!" );
		}
		String eMessage = null;
		do {
			System.out.print( eliza );
			if ( eMessage != null ) {
				System.out.println( theEliza.processInput( eMessage.trim() ) );
			}
			else {
				System.out.println( ":-)" );
			}
			System.out.print( you );
			eMessage = theConsole.readLine();
		} while ( !_language.isExitKeyword( eMessage ) );
		System.exit( 0 );
	}

	// /////////////////////////////////////////////////////////////////////////
	// SERVER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Starts the server.
	 * 
	 * @throws IOException thrown in case I/O related problems occurred.
	 */
	public void startServer() throws IOException {
		_server.open( _port );
		if ( !_isQuiet ) {
			LOGGER.info( "Server started on port <" + _server.getPort() + "> ..." );
		}
		else {
			System.out.println( "Server started on port <" + _server.getPort() + "> ..." );
		}
	}

	/**
	 * Stops the server.
	 * 
	 * @throws IOException thrown in case I/O related problems occurred.
	 */
	public void stopServer() throws IOException {
		_server.close();
		if ( !_isQuiet ) {
			LOGGER.info( "Server on port <" + _server.getPort() + "> stopped!" );
		}
		else {
			System.out.println( "Server on port <" + _server.getPort() + "> stopped!" );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Registers a lambda handling new conversation session request.
	 * 
	 * @param aBasePath The base path to be used.
	 * @param aServer The server to register the lambda to.
	 * 
	 * @throws IOException thrown in case there were I/O related problems.
	 */
	private void onCreateConversationSession( RestfulHttpServer aServer, String aBasePath ) throws IOException {
		aServer.onPut( aBasePath + "/sessions", ( aRequest, aResponse ) -> {
			final ElizaRequest theElizaRequest = aRequest.getRequest( ElizaRequest.class );
			if ( !_isQuiet ) {
				LOGGER.info( "Received new conversation session request \"" + theElizaRequest.getMessage() + "\"..." );
			}
			final ServerSessionElizaResponse theBody = _service.createConversationSession( aRequest.getHeaderFields().getAcceptLanguages(), theElizaRequest );
			aResponse.setResponse( theBody );
		} ).withOpen();

	}

	/**
	 * Registers a lambda handling continue conversation session request.
	 * 
	 * @param aBasePath The base path to be used.
	 * @param aServer The server to register the lambda to.
	 * 
	 * @throws IOException thrown in case there were I/O related problems.
	 */
	private void onContinueConversationForSession( RestfulHttpServer aServer, String aBasePath ) throws IOException {
		aServer.onPost( aBasePath + "/sessions/${sessionId}", ( aRequest, aResponse ) -> {
			final String theSessionId = aRequest.getWildcardReplacement( "sessionId" );
			final ElizaRequest theElizaRequest = aRequest.getRequest( ElizaRequest.class );
			if ( !_isQuiet ) {
				LOGGER.info( "Received continue conversation session request \"" + theElizaRequest.getMessage() + "\" for session <" + theSessionId + "> ..." );
			}
			final ServerSessionElizaResponse theBody = _service.continueConversationForSession( theSessionId, theElizaRequest );
			aResponse.setResponse( theBody );

		} ).withOpen();
	}

	/**
	 * Registers a lambda handling new conversation state request.
	 * 
	 * @param aBasePath The base path to be used.
	 * @param aServer The server to register the lambda to.
	 * 
	 * @throws IOException thrown in case there were I/O related problems.
	 */
	private void onCreateConversationState( RestfulHttpServer aServer, String aBasePath ) throws IOException {
		aServer.onPut( aBasePath, ( aRequest, aResponse ) -> {
			final ElizaRequest theElizaRequest = aRequest.getRequest( ElizaRequest.class );
			if ( !_isQuiet ) {
				LOGGER.info( "Received create conversation state request \"" + theElizaRequest.getMessage() + "\"..." );
			}
			final ClientStateElizaResponse theBody = _service.createConversationState( aRequest.getHeaderFields().getAcceptLanguages(), theElizaRequest );
			aResponse.setResponse( theBody );

		} ).withOpen();
	}

	/**
	 * Registers a lambda handling new conversation state request.
	 * 
	 * @param aBasePath The base path to be used.
	 * @param aServer The server to register the lambda to.
	 * 
	 * @throws IOException thrown in case there were I/O related problems.
	 */
	private void onContinueConversationForState( RestfulHttpServer aServer, String aBasePath ) throws IOException {
		aServer.onPost( aBasePath, ( aRequest, aResponse ) -> {
			final ClientStateElizaRequest theElizaRequest = aRequest.getRequest( ClientStateElizaRequest.class );
			if ( !_isQuiet ) {
				LOGGER.info( "Received continue conversation state request \"" + theElizaRequest.getMessage() + "\" for provided state..." );
			}
			final ClientStateElizaResponse theBody = _service.continueConversationForState( theElizaRequest );
			aResponse.setResponse( theBody );

		} ).withOpen();
	}

	/**
	 * Registers a lambda handling new conversation state request.
	 * 
	 * @param aBasePath The base path to be used.
	 * @param aServer The server to register the lambda to.
	 * 
	 * @throws IOException thrown in case there were I/O related problems.
	 */
	private void onTeamsConversation( RestfulHttpServer aServer, String aBasePath ) throws IOException {
		aServer.onPost( aBasePath + "/teams", ( aRequest, aResponse ) -> {
			final TeamsConversationRequest theTeamsRequest = aRequest.getRequest( TeamsConversationRequest.class );
			if ( !_isQuiet ) {
				LOGGER.info( "Received continue conversation state request \"" + theTeamsRequest.getText() + "\" for provided state..." );
			}
			final String theSessionId = theTeamsRequest.getConversation().getId();
			final String theMessage = theTeamsRequest.toMessage();
			final ServerSessionElizaResponse theResponse;
			if ( _service.hasConversationSession( theSessionId ) ) {
				theResponse = _service.continueConversationForSession( theSessionId, theMessage );
			}
			else {
				final List<Locale> theAcceptLanguages = new ArrayList<>();
				theAcceptLanguages.add( theTeamsRequest.toLocale() );
				if ( aRequest.getHeaderFields().getAcceptLanguages() != null ) {
					theAcceptLanguages.addAll( aRequest.getHeaderFields().getAcceptLanguages() );
				}
				theResponse = _service.createConversationSession( theAcceptLanguages, theSessionId, theMessage );
			}

			aResponse.setResponse( new TeamsConversationResponse( theResponse.getAnswer() ) );

		} ).withOpen();
	}

	/**
	 * Install the exception handler:
	 * 
	 * @param aServer The {@link RestfulHttpServer} to use.
	 */
	private void onHttpException( RestfulHttpServer aServer ) {
		aServer.onHttpException( ( RestRequestEvent aRequestEvent, HttpServerResponse aHttpServerResponse, Exception aException, HttpStatusCode aHttpStatusCode ) -> {
			if ( !_isQuiet ) {
				LOGGER.warn( "Got a <" + aException.getClass().getSimpleName() + "> exception, responding with HTTP-Status-Code <" + aHttpStatusCode + "> (" + aHttpStatusCode.getStatusCode() + "> as of: " + Trap.asMessage( aException ) );
			}
		} );
	}
}