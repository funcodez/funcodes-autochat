// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

/**
 * Eliza decomposition rule
 */
public class Decomp {

	private String _pattern; // The decomp pattern
	private boolean _mem; // The mem flag
	private ReasembList _reasemb; // The reassembly list
	private int _currReasmb; // The current reassembly point

	/**
	 * Initialize the decomp rule
	 */
	Decomp( String pattern, boolean mem, ReasembList reasemb ) {
		_pattern = pattern;
		_mem = mem;
		_reasemb = reasemb;
		_currReasmb = 100;
	}

	/**
	 * Print out the decomp rule.
	 *
	 * @param indent the indent
	 */
	public void print( int indent ) {
		String m = _mem ? "true" : "false";
		for ( int i = 0; i < indent; i++ ) {
			System.out.print( " " );
		}
		System.out.println( "decomp: " + _pattern + " " + m );
		_reasemb.print( indent + 2 );
	}

	/**
	 * Get the pattern.
	 *
	 * @return the string
	 */
	public String pattern() {
		return _pattern;
	}

	/**
	 * Get the mem flag.
	 *
	 * @return true, if successful
	 */
	public boolean mem() {
		return _mem;
	}

	/**
	 * Get the next reassembly rule.
	 *
	 * @return the string
	 */
	public String nextRule() {
		if ( _reasemb.size() == 0 ) {
			System.out.println( "No reassembly rule." );
			return null;
		}
		return (String) _reasemb.elementAt( _currReasmb );
	}

	/**
	 * Step to the next reassembly rule. If mem is true, pick a random rule.
	 */
	public void stepRule() {
		int size = _reasemb.size();
		if ( _mem ) {
			_currReasmb = (int) ( Math.random() * size );
		}
		//  Increment and make sure it is within range.
		_currReasmb++;
		if ( _currReasmb >= size ) {
			_currReasmb = 0;
		}
	}
}
