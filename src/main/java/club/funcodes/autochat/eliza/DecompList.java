// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

import java.util.Vector;

/**
 * Eliza decomp list. This stores all the decompositions of a single key.
 */
public class DecompList extends Vector<Decomp> {

	private static final long serialVersionUID = 1L;

	/**
	 * Add another decomp rule to the list.
	 *
	 * @param word the word
	 * @param mem the mem
	 * @param reasmb the reasmb
	 */
	public void add( String word, boolean mem, ReasembList reasmb ) {
		addElement( new Decomp( word, mem, reasmb ) );
	}

	/**
	 * Print the whole decomp list.
	 *
	 * @param indent the indent
	 */
	public void print( int indent ) {
		for ( int i = 0; i < size(); i++ ) {
			Decomp d = (Decomp) elementAt( i );
			d.print( indent );
		}
	}
}
