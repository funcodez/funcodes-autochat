// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Eliza main class. Stores the processed script. Does the input
 * transformations.
 */
public class Eliza {

	static final String DEFAULT_ELIZA_SCRIPT = "eliza.script";

	private boolean _finished = false;
	private String _finl = "Goodbye.";
	private String _initial = "Hello.";
	private KeyList _keys = new KeyList();
	private KeyStack _keyStack = new KeyStack();
	private DecompList _lastDecomp;
	private ReasembList _lastReasemb;
	private Mem _mem = new Mem();
	private PrePostList _post = new PrePostList();
	private PrePostList _pre = new PrePostList();
	private final boolean _printInitialFinal = false;
	private final boolean _printKeys = false;
	private final boolean _printPrePost = false;
	private final boolean _printSyns = false;
	private WordList _quit = new WordList();
	private SynList _syns = new SynList();

	/**
	 * Constructs an empty Eliza. Load an according script or invoke
	 * {@link #readDefaultScript()}.
	 */
	protected Eliza() {}

	/**
	 * Instantiates a new eliza.
	 *
	 * @param aScriptName the script name
	 */
	public Eliza( String aScriptName ) {
		try {
			if ( !readScript( aScriptName ) ) {
				throw new IllegalArgumentException( "Cannot read any content from script <" + aScriptName + ">!" );
			}
		}
		catch ( IOException e ) {
			new IllegalArgumentException( "Cannot read script <" + aScriptName + ">!", e );
		}
	}

	/**
	 * Process a line of script input.
	 *
	 * @param aLine the line
	 */
	public void collect( String aLine ) {
		String lines[] = new String[4];

		if ( EString.match( aLine, "*reasmb: *", lines ) ) {
			if ( _lastReasemb == null ) {
				System.out.println( "Error: no last reasemb" );
				return;
			}
			_lastReasemb.add( lines[1] );
		}
		else if ( EString.match( aLine, "*decomp: *", lines ) ) {
			if ( _lastDecomp == null ) {
				System.out.println( "Error: no last decomp" );
				return;
			}
			_lastReasemb = new ReasembList();
			String temp = new String( lines[1] );
			if ( EString.match( temp, "$ *", lines ) ) {
				_lastDecomp.add( lines[0], true, _lastReasemb );
			}
			else {
				_lastDecomp.add( temp, false, _lastReasemb );
			}
		}
		else if ( EString.match( aLine, "*key: * #*", lines ) ) {
			_lastDecomp = new DecompList();
			_lastReasemb = null;
			int n = 0;
			if ( lines[2].length() != 0 ) {
				try {
					n = Integer.parseInt( lines[2] );
				}
				catch ( NumberFormatException e ) {
					System.out.println( "Number is wrong in key: " + lines[2] );
				}
			}
			_keys.add( lines[1], n, _lastDecomp );
		}
		else if ( EString.match( aLine, "*key: *", lines ) ) {
			_lastDecomp = new DecompList();
			_lastReasemb = null;
			_keys.add( lines[1], 0, _lastDecomp );
		}
		else if ( EString.match( aLine, "*synon: * *", lines ) ) {
			WordList words = new WordList();
			words.add( lines[1] );
			aLine = lines[2];
			while ( EString.match( aLine, "* *", lines ) ) {
				words.add( lines[0] );
				aLine = lines[1];
			}
			words.add( aLine );
			_syns.add( words );
		}
		else if ( EString.match( aLine, "*pre: * *", lines ) ) {
			_pre.add( lines[1], lines[2] );
		}
		else if ( EString.match( aLine, "*post: * *", lines ) ) {
			_post.add( lines[1], lines[2] );
		}
		else if ( EString.match( aLine, "*initial: *", lines ) ) {
			_initial = lines[1];
		}
		else if ( EString.match( aLine, "*final: *", lines ) ) {
			_finl = lines[1];
		}
		else if ( EString.match( aLine, "*quit: *", lines ) ) {
			_quit.add( " " + lines[1] + " " );
		}
		else {
			System.out.println( "Unrecognized input: " + aLine );
		}
	}

	/**
	 * Finished.
	 *
	 * @return true, if successful
	 */
	public boolean finished() {
		return _finished;
	}

	/**
	 * Parses the script.
	 *
	 * @param aScript the script
	 * 
	 * @return true, if successful
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean parseScript( String aScript ) throws IOException {
		clearScript();

		String[] lines = aScript.split( "\\r?\\n" );
		if ( lines == null || lines.length == 0 ) {
			System.err.println( "Cannot parse Eliza script!" );
			return false;
		}
		else {
			for ( int i = 0; i < lines.length; i++ ) {
				collect( lines[i] );
			}
			return true;
		}
	}

	/**
	 * Print the stored script.
	 */
	public void print() {
		if ( _printKeys ) {
			_keys.print( 0 );
		}
		if ( _printSyns ) {
			_syns.print( 0 );
		}
		if ( _printPrePost ) {
			_pre.print( 0 );
			_post.print( 0 );
		}
		if ( _printInitialFinal ) {
			System.out.println( "initial: " + _initial );
			System.out.println( "final:   " + _finl );
			_quit.print( 0 );
			_quit.print( 0 );
		}
	}

	/**
	 * Process a line of input.
	 *
	 * @param aLine the s
	 * 
	 * @return the string
	 */
	public String processInput( String aLine ) {
		String reply;
		//  Do some input transformations first.
		aLine = EString.translate( aLine, "ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ", "abcdefghijklmnopqrstuvwxyzäöü" );
		aLine = EString.translate( aLine, "@#$%^&*()_-+=~`{[}]|:;<>\\\"", "                          " );
		aLine = EString.translate( aLine, ",?!", "..." );
		//  Compress out multiple speace.
		aLine = EString.compress( aLine );
		String lines[] = new String[2];
		//  Break apart sentences, and do each separately.
		while ( EString.match( aLine, "*.*", lines ) ) {
			reply = sentence( lines[0] );
			if ( reply != null ) {
				return reply.replaceAll( "  ", " " ).replaceAll( " ,", "," );
			}
			aLine = EString.trim( lines[1] );
		}
		if ( aLine.length() != 0 ) {
			reply = sentence( aLine );
			if ( reply != null ) {
				return reply.replaceAll( "  ", " " ).replaceAll( " ,", "," );
			}
		}
		//  Nothing matched, so try memory.
		String m = _mem.get();
		if ( m != null ) {
			return m;
		}

		//  No memory, reply with xnone.
		Key key = _keys.getKey( "xnone" );
		if ( key != null ) {
			Key dummy = null;
			reply = decompose( key, aLine, dummy );
			if ( reply != null ) {
				return reply.replaceAll( "  ", " " ).replaceAll( " ,", "," );
			}
		}
		//  No xnone, just say anything.
		return "I am at a loss for words.";
	}

	/**
	 * Read default script.
	 *
	 * @return true, if successful
	 */
	public boolean readDefaultScript() {
		clearScript();
		try {
			return readScript( DEFAULT_ELIZA_SCRIPT );
		}
		catch ( IOException e ) {
			throw new RuntimeException( "Cannot read default script <" + DEFAULT_ELIZA_SCRIPT + ">!", e );
		}
	}

	/**
	 * Reads the given script configuring {@link Eliza}.
	 * 
	 * @param aScriptName The name of the script.
	 * 
	 * @return True in case no script has been loaded (no script provided).
	 * 
	 * @throws IOException thrown in case reading the script caused an I/O
	 *         problem.
	 */
	public boolean readScript( String aScriptName ) throws IOException {
		clearScript();
		String[] script = readLines( aScriptName );
		if ( script == null || script.length == 0 ) {
			System.err.println( "Cannot load Eliza script!" );
			return false;
		}
		else {
			initScript( script );
			return true;
		}
	}

	// -------------------------------------------------------------------------
	// HOOKS:
	// -------------------------------------------------------------------------

	/**
	 * Initializes the script of {@link Eliza} with the provided lines
	 * representing the conversion phrases script.
	 * 
	 * @param aScript The script lines with which to initialize.
	 */
	protected void initScript( String[] aScript ) {
		for ( int i = 0; i < aScript.length; i++ ) {
			collect( aScript[i] );
		}
	}

	/**
	 * Reads the text referenced by the given file name.
	 * 
	 * @param aFileName The file name for the text to load.
	 * 
	 * @return An array of {@link String} instances, with each element in the
	 *         array representing one line of the text.
	 * 
	 * @throws IOException in case there were I/O problems loading the text.
	 */
	protected static String[] readLines( String aFileName ) throws IOException {
		File file = new File( aFileName );
		InputStream resourceAsStream = Eliza.class.getClassLoader().getResourceAsStream( aFileName );
		try ( BufferedReader reader = file.exists() && file.isFile() ? new BufferedReader( new FileReader( file ) ) : new BufferedReader( new InputStreamReader( resourceAsStream, StandardCharsets.UTF_8 ) ) ) {
			String line;
			List<String> list = new ArrayList<>();
			while ( ( line = reader.readLine() ) != null ) {
				list.add( line );
			}
			return list.toArray( new String[list.size()] );
		}
	}

	// -------------------------------------------------------------------------
	// HELPER:
	// -------------------------------------------------------------------------

	/**
	 * Assembly a reply from a decomp rule and the input. If the reassembly rule
	 * is goto, return null and give the gotoKey to use. Otherwise return the
	 * response.
	 */
	private String assemble( Decomp aDecompRule, String aReply[], Key aGotoKey ) {
		String lines[] = new String[3];
		aDecompRule.stepRule();
		String rule = aDecompRule.nextRule();
		if ( EString.match( rule, "goto *", lines ) ) {
			//  goto rule -- set gotoKey and return false.
			aGotoKey.copy( _keys.getKey( lines[0] ) );
			if ( aGotoKey.getKey() != null ) {
				return null;
			}
			System.out.println( "Goto rule did not match key: " + lines[0] );
			return null;
		}
		String work = "";
		while ( EString.match( rule, "* (#)*", lines ) ) {
			//  reassembly rule with number substitution
			rule = lines[2]; // there might be more
			int n = 0;
			try {
				n = Integer.parseInt( lines[1] ) - 1;
			}
			catch ( NumberFormatException e ) {
				System.out.println( "Number is wrong in reassembly rule " + lines[1] );
			}
			if ( n < 0 || n >= aReply.length ) {
				System.out.println( "Substitution number is bad " + lines[1] );
				return null;
			}
			aReply[n] = _post.translate( aReply[n] );
			work += lines[0] + " " + aReply[n];
		}
		work += rule;
		if ( aDecompRule.mem() ) {
			_mem.save( work );
			return null;
		}
		return work;
	}

	private void clearScript() {
		_keys.clear();
		_syns.clear();
		_pre.clear();
		_post.clear();
		_initial = "";
		_finl = "";
		_quit.clear();
		_keyStack.reset();
	}

	/**
	 * Decompose a string according to the given key. Try each decomposition
	 * rule in order. If it matches, assemble a reply and return it. If assembly
	 * fails, try another decomposition rule. If assembly is a goto rule, return
	 * null and give the key. If assembly succeeds, return the reply;
	 */
	private String decompose( Key aKey, String aSentence, Key aGotoKey ) {
		String reply[] = new String[10];
		for ( int i = 0; i < aKey.decomp().size(); i++ ) {
			Decomp d = aKey.decomp().elementAt( i );
			String pat = d.pattern();
			if ( _syns.matchDecomp( aSentence, pat, reply ) ) {
				String rep = assemble( d, reply, aGotoKey );
				if ( rep != null ) {
					return rep;
				}
				if ( aGotoKey.getKey() != null ) {
					return null;
				}
			}
		}
		return null;
	}

	/**
	 * Process a sentence. (1) Make pre transformations. (2) Check for quit
	 * word. (3) Scan sentence for keys, build key stack. (4) Try decompositions
	 * for each key.
	 */
	private String sentence( String aSentence ) {
		aSentence = _pre.translate( aSentence );
		aSentence = EString.pad( aSentence );
		if ( _quit.find( aSentence ) ) {
			_finished = true;
			return _finl;
		}
		_keys.buildKeyStack( _keyStack, aSentence );
		for ( int i = 0; i < _keyStack.keyTop(); i++ ) {
			Key gotoKey = new Key();
			String reply = decompose( _keyStack.key( i ), aSentence, gotoKey );
			if ( reply != null ) {
				return reply;
			}
			//  If decomposition returned gotoKey, try it
			while ( gotoKey.getKey() != null ) {
				reply = decompose( gotoKey, aSentence, gotoKey );
				if ( reply != null ) {
					return reply;
				}
			}
		}
		return null;
	}
}
