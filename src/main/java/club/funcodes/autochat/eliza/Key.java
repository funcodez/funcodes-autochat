// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

/**
 * Eliza key. A key has the key itself, a rank, and a list of decompositon
 * rules.
 */
public class Key {

	private String _key; // The key itself
	private int _rank; // The numerical rank
	private DecompList _decomp; // The list of decompositions

	/**
	 * Initialize the key.
	 */
	Key( String key, int rank, DecompList decomp ) {
		this._key = key;
		this._rank = rank;
		this._decomp = decomp;
	}

	/**
	 * Another initialization for gotoKey.
	 */
	Key() {
		_key = null;
		_rank = 0;
		_decomp = null;
	}

	/**
	 * Copy.
	 *
	 * @param k the k
	 */
	public void copy( Key k ) {
		_key = k.getKey();
		_rank = k.getRank();
		_decomp = k.decomp();
	}

	/**
	 * Print the key and all under it.
	 *
	 * @param indent the indent
	 */
	public void print( int indent ) {
		for ( int i = 0; i < indent; i++ ) {
			System.out.print( " " );
		}
		System.out.println( "key: " + _key + " " + _rank );
		_decomp.print( indent + 2 );
	}

	/**
	 * Print the key and rank only, not the rest.
	 *
	 * @param indent the indent
	 */
	public void printKey( int indent ) {
		for ( int i = 0; i < indent; i++ ) {
			System.out.print( " " );
		}
		System.out.println( "key: " + _key + " " + _rank );
	}

	/**
	 * Get the key value.
	 *
	 * @return the string
	 */
	public String getKey() {
		return _key;
	}

	/**
	 * Get the rank.
	 *
	 * @return the int
	 */
	public int getRank() {
		return _rank;
	}

	/**
	 * Get the decomposition list.
	 *
	 * @return the decomp list
	 */
	public DecompList decomp() {
		return _decomp;
	}
}
