// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

import java.util.Vector;

/**
 * Eliza key list. This stores all the keys.
 */
public class KeyList extends Vector<Key> {

	private static final long serialVersionUID = 1L;

	/**
	 * Add a new key.
	 *
	 * @param aKey the key
	 * @param aRank the rank
	 * @param aDecomp the decomp
	 */
	public void add( String aKey, int aRank, DecompList aDecomp ) {
		addElement( new Key( aKey, aRank, aDecomp ) );
	}

	/**
	 * Print all the keys.
	 *
	 * @param aIndent the indent
	 */
	public void print( int aIndent ) {
		for ( int i = 0; i < size(); i++ ) {
			Key k = (Key) elementAt( i );
			k.print( aIndent );
		}
	}

	/**
	 * Search the key list for a given key. Return the Key if found, else null.
	 */
	Key getKey( String aKey ) {
		for ( int i = 0; i < size(); i++ ) {
			Key key = (Key) elementAt( i );
			if ( aKey.equals( key.getKey() ) ) {
				return key;
			}
		}
		return null;
	}

	/**
	 * Break the string s into words. For each word, if isKey is true, then push
	 * the key into the stack.
	 *
	 * @param aStack the stack
	 * @param aWords the words
	 */
	public void buildKeyStack( KeyStack aStack, String aWords ) {
		aStack.reset();
		aWords = EString.trim( aWords );
		String lines[] = new String[2];
		Key k;
		while ( EString.match( aWords, "* *", lines ) ) {
			k = getKey( lines[0] );
			if ( k != null ) {
				aStack.pushKey( k );
			}
			aWords = lines[1];
		}
		k = getKey( aWords );
		if ( k != null ) {
			aStack.pushKey( k );
			//stack.print();
		}
	}
}
