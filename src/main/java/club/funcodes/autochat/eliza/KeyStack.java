// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

/**
 * A stack of keys. The keys are kept in rank order.
 */
public class KeyStack {

	private static final int STACK_SIZE = 20; // The stack size
	private Key _keyStack[] = new Key[STACK_SIZE]; // The key stack
	private int _keyTop = 0; // The top of the key stack

	/**
	 * Prints the key stack.
	 */
	public void print() {
		System.out.println( "Key stack " + _keyTop );
		for ( int i = 0; i < _keyTop; i++ ) {
			_keyStack[i].printKey( 0 );
		}
	}

	/**
	 * Get the stack size.
	 *
	 * @return the int
	 */
	public int keyTop() {
		return _keyTop;
	}

	/**
	 * Reset the key stack.
	 */
	public void reset() {
		_keyTop = 0;
	}

	/**
	 * Get a key from the stack.
	 *
	 * @param n the n
	 * 
	 * @return the key
	 */
	public Key key( int n ) {
		return n < 0 || n >= _keyTop ? null : _keyStack[n];
	}

	/**
	 * Push a key in the stack. Keep the highest rank keys at the bottom.
	 *
	 * @param key the key
	 */
	public void pushKey( Key key ) {
		if ( key == null ) {
			System.out.println( "push null key" );
			return;
		}
		int i;
		for ( i = _keyTop; i > 0; i-- ) {
			if ( key.getRank() > _keyStack[i - 1].getRank() ) {
				_keyStack[i] = _keyStack[i - 1];
			}
			else {
				break;
			}
		}
		_keyStack[i] = key;
		_keyTop++;
	}
}
