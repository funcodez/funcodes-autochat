// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

/**
 * Eliza memory class
 */
public class Mem {

	private static final int MEM_MAX = 20; // The memory size

	private String _memory[] = new String[MEM_MAX]; // The memory
	private int _memTop = 0; // The memory top 

	/**
	 * Save the element at the end.
	 *
	 * @param aElement the element to be saved
	 */
	public void save( String aElement ) {
		if ( _memTop < MEM_MAX ) {
			_memory[_memTop++] = new String( aElement );
		}
	}

	/**
	 * Gets the first element.
	 *
	 * @return the first element
	 */
	public String get() {
		if ( _memTop == 0 ) {
			return null;
		}
		String m = _memory[0];
		for ( int i = 0; i < _memTop - 1; i++ ) {
			_memory[i] = _memory[i + 1];
		}
		_memTop--;
		return m;
	}
}
