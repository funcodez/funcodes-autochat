// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

/**
 * Eliza pre-post entry (two words). This is used to store pre transforms or
 * post transforms.
 */
public class PrePost {

	String _src; // The words
	String _dest; // The words

	/**
	 * Initialize the pre-post entry.
	 */
	PrePost( String aSrc, String aDest ) {
		_src = aSrc;
		_dest = aDest;
	}

	/**
	 * Print the pre-post entry.
	 *
	 * @param aIndent the indent
	 */
	public void print( int aIndent ) {
		for ( int i = 0; i < aIndent; i++ ) {
			System.out.print( " " );
		}
		System.out.println( "pre-post: " + _src + "  " + _dest );
	}

	/**
	 * Get src.
	 *
	 * @return the string
	 */
	public String getSrc() {
		return _src;
	}

	/**
	 * Get dest.
	 *
	 * @return the string
	 */
	public String getDest() {
		return _dest;
	}
}
