// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

import java.util.Vector;

/**
 * Eliza prePost list. This list of pre-post entries is used to perform word
 * transformations prior to or after other processing.
 */
public class PrePostList extends Vector<PrePost> {

	private static final long serialVersionUID = 1L;

	/**
	 * Add another entry to the list.
	 *
	 * @param aSrc the src
	 * @param aDest the dest
	 */
	public void add( String aSrc, String aDest ) {
		addElement( new PrePost( aSrc, aDest ) );
	}

	/**
	 * Prnt the pre-post list.
	 *
	 * @param aIndent the indent
	 */
	public void print( int aIndent ) {
		for ( int i = 0; i < size(); i++ ) {
			PrePost p = (PrePost) elementAt( i );
			p.print( aIndent );
		}
	}

	/**
	 * Translate a string. If str matches a src string on the list, return he
	 * corresponding dest. If no match, return the input.
	 */
	String xlate( String aStr ) {
		for ( int i = 0; i < size(); i++ ) {
			PrePost p = (PrePost) elementAt( i );
			if ( aStr.equals( p.getSrc() ) ) {
				return p.getDest();
			}
		}
		return aStr;
	}

	/**
	 * Translate a string s. (1) Trim spaces off. (2) Break s into words. (3)
	 * For each word, substitute matching src word with dest.
	 *
	 * @param aString the s
	 * 
	 * @return the string
	 */
	public String translate( String aString ) {
		String lines[] = new String[2];
		String work = EString.trim( aString );
		aString = "";
		while ( EString.match( work, "* *", lines ) ) {
			aString += xlate( lines[0] ) + " ";
			work = EString.trim( lines[1] );
		}
		aString += xlate( work );
		return aString;
	}
}
