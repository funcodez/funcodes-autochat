// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

import java.util.Vector;

/**
 * Eliza reassembly list.
 */
public class ReasembList extends Vector<String> {

	private static final long serialVersionUID = 1L;

	/**
	 * Add an element to the reassembly list.
	 *
	 * @param reasmb the reasmb
	 * 
	 * @return True in case the {@link String} has been added (always).
	 */
	public boolean add( String reasmb ) {
		addElement( reasmb );
		return true;
	}

	/**
	 * Print the reassembly list.
	 *
	 * @param indent the indent
	 */
	public void print( int indent ) {
		for ( int i = 0; i < size(); i++ ) {
			for ( int j = 0; j < indent; j++ ) {
				System.out.print( " " );
			}
			String s = (String) elementAt( i );
			System.out.println( "reasemb: " + s );
		}
	}
}
