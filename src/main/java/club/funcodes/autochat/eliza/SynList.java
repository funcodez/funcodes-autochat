// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

import java.util.Vector;

/**
 * Eliza synonym list. Collection of all the synonym elements.
 */
public class SynList extends Vector<WordList> {

	private static final long serialVersionUID = 1L;

	/**
	 * Add another word list the the synonym list.
	 *
	 * @param aWords the words
	 * 
	 * @return true, if successful
	 */
	public boolean add( WordList aWords ) {
		addElement( aWords );
		return true;
	}

	/**
	 * Prnt the synonym lists.
	 *
	 * @param aIndent the indent
	 */
	public void print( int aIndent ) {
		for ( int i = 0; i < size(); i++ ) {
			for ( int j = 0; j < aIndent; j++ ) {
				System.out.print( " " );
			}
			System.out.print( "synon: " );
			WordList w = (WordList) elementAt( i );
			w.print( aIndent );
		}
	}

	/**
	 * Find a synonym word list given the any word in it.
	 *
	 * @param aSynonyms the s
	 * 
	 * @return the word list
	 */
	public WordList find( String aSynonyms ) {
		for ( int i = 0; i < size(); i++ ) {
			WordList w = (WordList) elementAt( i );
			if ( w.find( aSynonyms ) ) {
				return w;
			}
		}
		return null;
	}

	/**
	 * Decomposition match, If decomp has no synonyms, do a regular match.
	 * Otherwise, try all synonyms.
	 */
	boolean matchDecomp( String aStr, String aPat, String aLines[] ) {
		if ( !EString.match( aPat, "*@* *", aLines ) ) {
			//  no synonyms in decomp pattern
			return EString.match( aStr, aPat, aLines );
		}
		//  Decomp pattern has synonym -- isolate the synonym
		String first = aLines[0];
		String synWord = aLines[1];
		String theRest = " " + aLines[2];
		//  Look up the synonym
		WordList syn = find( synWord );
		if ( syn == null ) {
			System.out.println( "Could not fnd syn list for " + synWord );
			return false;
		}
		//  Try each synonym individually
		for ( int i = 0; i < syn.size(); i++ ) {
			//  Make a modified pattern
			aPat = first + (String) syn.elementAt( i ) + theRest;
			if ( EString.match( aStr, aPat, aLines ) ) {
				int n = EString.count( first, '*' );
				//  Make room for the synonym in the match list.
				for ( int j = aLines.length - 2; j >= n; j-- ) {
					aLines[j + 1] = aLines[j];
				}
				//  The synonym goes in the match list.
				aLines[n] = (String) syn.elementAt( i );
				return true;
			}
		}
		return false;
	}
}
