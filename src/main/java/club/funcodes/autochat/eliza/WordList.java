// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.eliza;

import java.util.Vector;

/**
 * Eliza word list.
 */
public class WordList extends Vector<String> {

	private static final long serialVersionUID = 1L;

	/**
	 * Add another word to the list.
	 *
	 * @param aWord the word
	 * 
	 * @return true, if successful
	 */
	public boolean add( String aWord ) {
		addElement( aWord );
		return true;
	}

	/**
	 * Print a word list on one line.
	 *
	 * @param aIndent the indent
	 */
	public void print( int aIndent ) {
		for ( int i = 0; i < size(); i++ ) {
			String s = (String) elementAt( i );
			System.out.print( s + "  " );
		}
		System.out.println();
	}

	/**
	 * Find a string in a word list. Return true if the word is in the list,
	 * false otherwise.
	 */
	boolean find( String aWord ) {
		for ( int i = 0; i < size(); i++ ) {
			if ( aWord.equals( (String) elementAt( i ) ) ) {
				return true;
			}
		}
		return false;
	}
}
