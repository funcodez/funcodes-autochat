// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
// "Communications of the ACM" in January 1966 [1]. CREDITS to the ELIZA imple-
// mentation in Java by CODEANTICODE [2] as well as to Charles Hayden's Java
// implementation of ELIZA [3] (being a complete and faithful implementation of
// the program described by Weizenbaum).
// -----------------------------------------------------------------------------
// [1] https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf
// [2] https://github.com/codeanticode/eliza
// [3] http://chayden.net/eliza/Eliza.html
// -----------------------------------------------------------------------------
// This code, as being based on [1], has been derived from [2] and [3] !
// /////////////////////////////////////////////////////////////////////////////

/**
 * ACKNOWLEDGMENTS to Joseph Weizenbaum's ELIZA psychologist described in the
 * Communications of the ACM in January 1966 [1]. CREDITS to the ELIZA
 * implementation in Java by CODEANTICODE [2] as well as to Charles Hayden's
 * Java implementation of ELIZA [3] (being a complete and faithful
 * implementation of the program described by Weizenbaum).
 * <p>
 * <ul>
 * <li>[1] "https://cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf"
 * <li>[2] "https://github.com/codeanticode/eliza"
 * <li>[3] "http://chayden.net/eliza/Eliza.html"
 * </ul>
 */
package club.funcodes.autochat.eliza;