package club.funcodes.autochat.service;

/**
 * An {@link ElizaRequest} also providing an {@link ElizaState}.
 */
public class ClientStateElizaRequest extends ElizaRequest {

	private ElizaState _state;

	/**
	 * Instantiates a new eliza state request.
	 */
	public ClientStateElizaRequest() {}

	/**
	 * Instantiates a new eliza state request.
	 *
	 * @param aMessage The message to be sent to ELiza.
	 * @param aElizaState The client side state of the Eliza session.
	 */
	public ClientStateElizaRequest( String aMessage, ElizaState aElizaState ) {
		super( aMessage );
		_state = aElizaState;
	}

	/**
	 * Instantiates a new eliza state request.
	 *
	 * @param aMessage The message to be sent to ELiza.
	 * @param aElizaState The client side state of the Eliza session.
	 */
	public ClientStateElizaRequest( String aMessage, String aElizaState ) {
		super( aMessage );
		_state = new ElizaState( aElizaState );
	}

	/**
	 * Gets the eliza state.
	 *
	 * @return the _state
	 */
	public ElizaState getState() {
		return _state;
	}

	/**
	 * Sets the eliza state.
	 *
	 * @param aState the _state to set
	 */
	public void setState( ElizaState aState ) {
		_state = aState;
	}
}
