package club.funcodes.autochat.service;

import club.funcodes.autochat.eliza.Eliza;

/**
 * A {@link ClientStateElizaResponse} encapsulates an {@link Eliza}'s answer as
 * well as its current state.
 */
public class ClientStateElizaResponse {

	private String answer;
	private ElizaState state;

	/**
	 * Instantiates a new client state eliza response.
	 */
	public ClientStateElizaResponse() {}

	/**
	 * Constructs {@link Eliza}'s processed answer as well as her current state.
	 * 
	 * @param aAnswer The last answer of {@link Eliza}.
	 * 
	 * @param aState {@link Eliza}'s current state.
	 */
	public ClientStateElizaResponse( String aAnswer, ElizaState aState ) {
		answer = aAnswer;
		state = aState;
	}

	/**
	 * Constructs {@link Eliza}'s processed answer as well as her current state.
	 * 
	 * @param aAnswer The last answer of {@link Eliza}.
	 * 
	 * @param aState {@link Eliza}'s current state.
	 */
	public ClientStateElizaResponse( String aAnswer, String aState ) {
		answer = aAnswer;
		state = new ElizaState( aState );
	}

	/**
	 * Gets the answer.
	 *
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * Sets the answer.
	 *
	 * @param aAnswer the answer to set
	 */
	public void setAnswer( String aAnswer ) {
		answer = aAnswer;
	}

	/**
	 * Sets the state.
	 *
	 * @param aState the state to set
	 */
	public void setState( ElizaState aState ) {
		state = aState;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public ElizaState getState() {
		return state;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [answer=" + answer + ", state=" + state + "]";
	}
}
