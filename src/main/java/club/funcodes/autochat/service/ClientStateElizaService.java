package club.funcodes.autochat.service;

import java.util.List;
import java.util.Locale;

/**
 * The {@link ClientStateElizaService} is an Eliza service where the state is
 * managed by the client. The service therewith is stateless.
 */
public interface ClientStateElizaService extends ElizaMessageHook {

	/**
	 * Continues a conversation initiated by
	 * {@link #createConversationState(List, String)} from the given
	 * {@link ElizaState} and the input text.
	 * 
	 * @param aStatefulRequest The {@link ElizaState} containing the current
	 *        course of the conversation and the text for continuing the
	 *        conversation.
	 * 
	 * @return The according {@link ClientStateElizaResponse} containing the
	 *         answer and the {@link ElizaState} (being the course of the
	 *         conversation).
	 */
	default ClientStateElizaResponse continueConversationForState( ClientStateElizaRequest aStatefulRequest ) {
		return continueConversationForState( aStatefulRequest.getState(), aStatefulRequest.getMessage() );
	}

	/**
	 * Continues a conversation initiated by
	 * {@link #createConversationState(List, String)} from the given
	 * {@link ElizaState} and the input text.
	 * 
	 * @param aState The {@link ElizaState} containing the current course of the
	 *        conversation.
	 * @param aMessage The text for continuing the conversation.
	 * 
	 * @return The according {@link ClientStateElizaResponse} containing the
	 *         answer and the {@link ElizaState} (being the course of the
	 *         conversation).
	 */
	default ClientStateElizaResponse continueConversationForState( ElizaState aState, String aMessage ) {
		ElizaState theState = aState;
		String theOutput = onMessageHook( aMessage );
		if ( theOutput == null ) {
			ElizaDecorator theEliza = new ElizaDecorator( aState );
			theOutput = theEliza.processInput( aMessage );
			theState = theEliza.toState();
		}
		return new ClientStateElizaResponse( theOutput, theState );
	}

	/**
	 * Create a new {@link ElizaState} from the given input text and supported
	 * {@link Locale} instances and return the according
	 * {@link ClientStateElizaResponse}. Useful for clients managing client-side
	 * state.
	 * 
	 * @param aLocales The {@link Locale} supported by the client.
	 * @param aMessage The greeting for initializing the {@link ElizaState}.
	 * 
	 * @return The according {@link ClientStateElizaResponse} containing the
	 *         answer and the {@link ElizaState} (being the course of the
	 *         conversation).
	 */
	default ClientStateElizaResponse createConversationState( List<Locale> aLocales, String aMessage ) {
		ElizaDecorator theEliza = new ElizaDecorator( aLocales );
		String theOutput = onMessageHook( aMessage );
		if ( theOutput == null ) {
			theOutput = theEliza.processInput( aMessage );
		}
		return new ClientStateElizaResponse( theOutput, theEliza.toState() );
	}

	/**
	 * Create a new {@link ElizaState} from the given input text and supported
	 * {@link Locale} instances and return the according
	 * {@link ClientStateElizaResponse}. Useful for clients managing client-side
	 * state.
	 * 
	 * @param aLocales The {@link Locale} supported by the client.
	 * @param aRequest The greeting for initializing the {@link ElizaState}.
	 * 
	 * @return The according {@link ClientStateElizaResponse} containing the
	 *         answer and the {@link ElizaState} (being the course of the
	 *         conversation).
	 */
	default ClientStateElizaResponse createConversationState( List<Locale> aLocales, ElizaRequest aRequest ) {
		return createConversationState( aLocales, aRequest.getMessage() );
	}
}
