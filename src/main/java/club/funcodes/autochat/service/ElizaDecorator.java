package club.funcodes.autochat.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import club.funcodes.autochat.eliza.Eliza;

/**
 * Decorates an {@link Eliza} instance with additional functionality regarding
 * language support and state support.
 */
public class ElizaDecorator extends Eliza {

	private List<String> input = new ArrayList<String>();
	private String[] script;

	// -------------------------------------------------------------------------
	// CONSTRUCTORSS:
	// -------------------------------------------------------------------------

	/**
	 * Constructs an {@link Eliza} instance for the first {@link Locale} being
	 * supported. If none is supported, then then {@link Locale#ENGLISH} is used
	 * (fallback).
	 * 
	 * @param aLocales The {@link Locale} list with the preferred locale setting
	 *        for {@link Eliza}. s
	 */
	public ElizaDecorator( List<Locale> aLocales ) {
		String eScriptName;
		out: {
			for ( Locale eLocale : aLocales ) {
				eScriptName = "eliza-" + eLocale.getLanguage() + ".script";
				try {
					if ( readScript( eScriptName ) ) {
						break out;
					}
				}
				catch ( IOException ignore ) {}
			}
			readDefaultScript();
		}
	}

	/**
	 * Constructs {@link Eliza} from the given {@link ElizaState} state.
	 * Reverses the {@link #toState()} method.
	 * 
	 * @param aState The state from which to construct {@link Eliza}.
	 */
	public ElizaDecorator( ElizaState aState ) {
		byte[] theBytes = Base64.getDecoder().decode( aState.getStateData() );
		try ( ByteArrayInputStream theByteIn = new ByteArrayInputStream( theBytes ); GZIPInputStream theZipIn = new GZIPInputStream( theByteIn ); ObjectInput theObjIn = new ObjectInputStream( theZipIn ) ) {
			initScript( (String[]) theObjIn.readObject() );
			readInput( (String[]) theObjIn.readObject() );
		}
		catch ( ClassNotFoundException | IOException e ) {
			throw new IllegalArgumentException( "Cannot recreate ELIZA state from the provided state <" + aState + ">!", e );
		}
	}

	// -------------------------------------------------------------------------
	// METHODS:
	// -------------------------------------------------------------------------

	/**
	 * Stores the current session's script by hooking into the super-class'
	 * {@link #initScript(String[])} method.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void initScript( String[] aScript ) {
		super.initScript( aScript );
		script = aScript;
	}

	/**
	 * Stores the current session's input state by hooking into the super-class'
	 * {@link #processInput(String)} method.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public String processInput( String s ) {
		input.add( s );
		return super.processInput( s );
	}

	/**
	 * Produces a state blob representing an {@link Eliza} instance.
	 * 
	 * @return The according {@link Eliza}'s conversational state.
	 */
	public ElizaState toState() {
		try ( ByteArrayOutputStream theByteOut = new ByteArrayOutputStream(); GZIPOutputStream theZipOut = new GZIPOutputStream( theByteOut ); ObjectOutput theObjOut = new ObjectOutputStream( theZipOut ) ) {
			theObjOut.writeObject( getScript() );
			theObjOut.writeObject( getInput() );
			theObjOut.flush();
			theObjOut.close();
			return new ElizaState( Base64.getEncoder().encodeToString( theByteOut.toByteArray() ) );
		}
		catch ( IOException e ) {
			throw new IllegalArgumentException( "Cannot write out ELIZA state!", e );
		}
	}

	// -------------------------------------------------------------------------
	// HOOKS:
	// -------------------------------------------------------------------------

	/**
	 * Initializes the conversation state of {@link Eliza} with the provided
	 * lines of input.
	 * 
	 * @param aInput The input lines with which to initialize.
	 */
	protected void readInput( String[] aInput ) {
		for ( String eInput : aInput ) {
			processInput( eInput );
		}
	}

	/**
	 * Retrieves the conversation state of {@link Eliza} with the provided lines
	 * of input.
	 * 
	 * @return The current state for the lines of input.
	 */
	protected String[] getInput() {
		return input.toArray( new String[input.size()] );
	}

	/**
	 * Retrieves the script of {@link Eliza} with the provided lines
	 * representing the conversion phrases script.
	 * 
	 * @return The script lines with which to initialize.
	 */
	protected String[] getScript() {
		return script;
	}
}
