// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.autochat.service;

import club.funcodes.autochat.eliza.Eliza;

/**
 * Interface defining a hook method to bypass {@link Eliza} in case a
 * "triggering" message is detected. By default, {@link Eliza} is invoked on any
 * incoming message. Useful if you want to intercept a message for which you
 * want to return some https://www.metacodes.pro information instead of
 * {@link Eliza}'s answer.
 */
public interface ElizaMessageHook {

	/**
	 * Hook to be overwritten by an implementation of the {@link ElizaService}
	 * in case of https://www.metacodes.pro handling of messages and bypassing
	 * {@link Eliza}.
	 * 
	 * @param aMessage The message with which the hook is called.
	 * 
	 * @return The output to return or null if the hook did not catch (e.g. the
	 *         hook''s response is to be ignored).
	 */
	default String onMessageHook( String aMessage ) {
		return null;
	}
}
