package club.funcodes.autochat.service;

/**
 * Simple message container for an Eliza request.
 */
public class ElizaRequest {

	private String message;

	/**
	 * Instantiates a new Eliza request.
	 */
	public ElizaRequest() {}

	/**
	 * Instantiates a new Eliza request.
	 *
	 * @param aMessage The message describing this exception.
	 */
	public ElizaRequest( String aMessage ) {
		message = aMessage;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param aMessage The message describing this exception. to set
	 */
	public void setMessage( String aMessage ) {
		message = aMessage;
	}
}
