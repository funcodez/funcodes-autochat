package club.funcodes.autochat.service;

/**
 * A simple "Business"-Service to be implemented by the micro-framework in
 * question (here we go for an Eliza "chatbot" service).
 */
public interface ElizaService extends ServerSessionElizaService, ClientStateElizaService {}