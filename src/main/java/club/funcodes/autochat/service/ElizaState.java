package club.funcodes.autochat.service;

import org.refcodes.cli.CliContext;

import club.funcodes.autochat.eliza.Eliza;

/**
 * The {@link ElizaState} contains the current state of a user's conversation
 * with an {@link Eliza} instance. Get the state of {@link Eliza} via
 * {@link Eliza#toState(CliContext)} and construct Eliza from that state as of
 * {@link Eliza#Eliza(ElizaState)}.
 * 
 * The last update time as of {@link #getLastUpdateTime()} is initialized
 * whenever an {@link ElizaState} is created and updated whenever the
 * {@link #setStateData(String)} method is called. Use {@link #isExpired()} to
 * test whether the {@link ElizaState} expired.
 */
public class ElizaState {

	private static final int MIN_SESSION_EXPIRATION_TIME_MILLIS = 1000 * 60 * 60 * 24 * 7; // 7 Days

	private String _stateData;
	private long _lastUpdateTime;

	/**
	 * Instantiates a new eliza state. The last update time as of
	 * {@link #getLastUpdateTime()} is set to the current time (as of
	 * {@link System#currentTimeMillis()}).
	 */
	public ElizaState() {
		_lastUpdateTime = System.currentTimeMillis();
	}

	/**
	 * Constructs the container for the {@link Eliza} state with the given state
	 * blob. The last update time as of {@link #getLastUpdateTime()} is set to
	 * the current time (as of {@link System#currentTimeMillis()}).
	 * 
	 * @param aStateBlob The blob representing the {@link Eliza} state.
	 */
	public ElizaState( String aStateBlob ) {
		_stateData = aStateBlob;
		_lastUpdateTime = System.currentTimeMillis();
	}

	/**
	 * Constructs the container for the {@link Eliza} state with the given state
	 * blob.
	 * 
	 * @param aStateBlob The blob representing the {@link Eliza} state.
	 * 
	 * @param aLastUpdateTime The last update time as of
	 *        {@link #getLastUpdateTime()} to be set.
	 */
	public ElizaState( String aStateBlob, long aLastUpdateTime ) {
		_stateData = aStateBlob;
		_lastUpdateTime = aLastUpdateTime;
	}

	/**
	 * Retrieves the {@link Eliza} state.
	 * 
	 * @return The state blob.
	 */
	public String getStateData() {
		return _stateData;
	}

	/**
	 * Sets the {@link Eliza} state. The last update time as of
	 * {@link #getLastUpdateTime()} is set to the current time (as of
	 * {@link System#currentTimeMillis()}).
	 * 
	 * @param aStateData The state blob.
	 */
	public void setStateData( String aStateData ) {
		_stateData = aStateData;
		_lastUpdateTime = System.currentTimeMillis();
	}

	/**
	 * Determines whether this state is expired as of being older than
	 * {@value #MIN_SESSION_EXPIRATION_TIME_MILLIS} milliseconds regarding the
	 * last update time (as of {@link #getLastUpdateTime()}).
	 *
	 * @return true, if is expired
	 */
	// @JsonIgnore
	public boolean isExpired() {
		return _lastUpdateTime + MIN_SESSION_EXPIRATION_TIME_MILLIS < System.currentTimeMillis();
	}

	/**
	 * Gets the last update time.
	 *
	 * @return the lastUpdateTime
	 */
	public long getLastUpdateTime() {
		return _lastUpdateTime;
	}

	/**
	 * Sets the last update time.
	 *
	 * @param aLastUpdateTime the lastUpdateTime to set
	 */
	public void setLastUpdateTime( long aLastUpdateTime ) {
		_lastUpdateTime = aLastUpdateTime;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		String theStateText = _stateData.length() > 23 ? _stateData.substring( 0, 20 ) + "..." : _stateData;
		return "ElizaState [_stateData=" + theStateText + ", expired=" + isExpired() + ", lastUpdateTime=" + _lastUpdateTime + "]";
	}
}
