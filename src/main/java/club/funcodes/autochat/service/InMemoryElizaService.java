package club.funcodes.autochat.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Very basic straight forward in-memory implementation of the
 * {@link ElizaService}.
 */
public class InMemoryElizaService implements ElizaService, AutoCloseable {

	private Map<String, ElizaState> _sessions = new HashMap<>();
	private ScheduledExecutorService _scheduler = Executors.newSingleThreadScheduledExecutor();

	/**
	 * Instantiates the {@link InMemoryElizaService}, cleaning up expired
	 * sessions as of {@link #MIN_SESSION_EXPIRATION_TIME_MILLIS} every 10
	 * minutes.
	 */
	public InMemoryElizaService() {
		_scheduler.scheduleAtFixedRate( this::doSessionCleanup, 10, 10, TimeUnit.MINUTES );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServerSessionElizaResponse createConversationSession( List<Locale> aLocales, String aMessage ) {
		return createConversationSession( aLocales, UUID.randomUUID().toString(), aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServerSessionElizaResponse continueConversationForSession( String aSessionId, String aMessage ) {
		String theOutput = onMessageHook( aMessage );
		if ( theOutput == null ) {
			ElizaState theState = _sessions.get( aSessionId );
			ElizaDecorator theEliza = new ElizaDecorator( theState );
			theOutput = theEliza.processInput( aMessage );
			_sessions.put( aSessionId, theEliza.toState() );
		}
		return new ServerSessionElizaResponse( theOutput, aSessionId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasConversationSession( String aSessionId ) {
		synchronized ( _sessions ) {
			return _sessions.containsKey( aSessionId );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServerSessionElizaResponse createConversationSession( List<Locale> aLocales, String aSessionId, String aMessage ) {
		synchronized ( _sessions ) {
			if ( hasConversationSession( aSessionId ) ) {
				throw new IllegalStateException( "The provided session with ID <" + aSessionId + "> is already in use." );
			}
			ElizaDecorator theEliza = new ElizaDecorator( aLocales );
			String theOutput = onMessageHook( aMessage );
			if ( theOutput == null ) {
				theOutput = theEliza.processInput( aMessage );
			}
			ClientStateElizaResponse theResponse = new ClientStateElizaResponse( theOutput, theEliza.toState() );
			_sessions.put( aSessionId, theResponse.getState() );
			return new ServerSessionElizaResponse( theResponse.getAnswer(), aSessionId );
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Verifies whether a session has expired.
	 * 
	 * In case the last verification time is been more than
	 * {@link #MIN_SESSION_VERIFICATION_INTERVAL_TIME_MILLIS} milliseconds ago,
	 * then all the sessions' {@link ElizaState#getUpdatedDate()} value is
	 * tested whether it older than {@link #MIN_SESSION_EXPIRATION_TIME_MILLIS}
	 * milliseconds. If this the case, the according sessions are invalidated
	 * (removed from the service).
	 */
	void doSessionCleanup() {
		synchronized ( _sessions ) {
			Set<String> theKeys = new HashSet<>( _sessions.keySet() ); // Allow concurrent modification of sessions map
			ElizaState eState;
			for ( String eKey : theKeys ) {
				eState = _sessions.get( eKey );
				if ( eState != null && eState.isExpired() ) {
					_sessions.remove( eKey );
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws Exception {
		_scheduler.shutdownNow();
	}
}