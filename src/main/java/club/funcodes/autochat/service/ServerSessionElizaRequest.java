package club.funcodes.autochat.service;

/**
 * An {@link ElizaRequest} also providing an Eliza session ID {@link String}.
 */
public class ServerSessionElizaRequest extends ElizaRequest {

	private String sessionId;

	/**
	 * Instantiates a new Eliza state request.
	 */
	public ServerSessionElizaRequest() {}

	/**
	 * Instantiates a new Eliza state request.
	 *
	 * @param aMessage The message to be sent to ELiza.
	 * @param aSessionId The client side state of the Eliza sessionId.
	 */
	public ServerSessionElizaRequest( String aMessage, String aSessionId ) {
		super( aMessage );
		sessionId = new String( aSessionId );
	}

	/**
	 * Gets the Eliza state.
	 *
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Sets the Eliza state.
	 *
	 * @param aSession the sessionId to set
	 */
	public void setSessionId( String aSessionId ) {
		sessionId = aSessionId;
	}
}
