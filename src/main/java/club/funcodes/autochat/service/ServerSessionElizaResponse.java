package club.funcodes.autochat.service;

/**
 * The Class ServerSessionElizaResponse.
 */
public class ServerSessionElizaResponse {

	private String answer;
	private String sessionId;

	/**
	 * Instantiates a new server session eliza response.
	 */
	public ServerSessionElizaResponse() {}

	/**
	 * Creates an Eliza session with the given session ID and answer.
	 * 
	 * @param aAnswer The produced answer.
	 * @param aSessionId The session ID representing the course of the
	 *        conversation.
	 */
	public ServerSessionElizaResponse( String aAnswer, String aSessionId ) {
		answer = aAnswer;
		sessionId = aSessionId;
	}

	/**
	 * The answer for input directed at Eliza.
	 * 
	 * @return The according answer.
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * The session ID represents the course of.
	 *
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "ServerSessionElizaResponse [answer=" + answer + ", sessionId=" + sessionId + "]";
	}
}
