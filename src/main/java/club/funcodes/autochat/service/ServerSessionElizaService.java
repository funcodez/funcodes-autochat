package club.funcodes.autochat.service;

import java.util.List;
import java.util.Locale;

/**
 * The {@link ServerSessionElizaService} is an Eliza service where the state is
 * managed by the server. The service therewith is stateful.
 */
public interface ServerSessionElizaService extends ElizaMessageHook {

	/**
	 * Continues a conversation initiated by
	 * {@link #createConversationSession(List, String)} from the given session
	 * ID and the input text.
	 * 
	 * @param aRequest The session ID representing the current course of the
	 *        conversation as well as the text for continuing the conversation.
	 * 
	 * @return The according {@link ServerSessionElizaResponse} containing the
	 *         answer and the session ID (representing the course of the
	 *         conversation).
	 * 
	 * @throws IllegalArgumentException in case the ID is unknown for an
	 *         existing session.
	 */
	default ServerSessionElizaResponse continueConversationForSession( ServerSessionElizaRequest aRequest ) {
		return continueConversationForSession( aRequest.getSessionId(), aRequest.getMessage() );
	}

	/**
	 * Continues a conversation initiated by
	 * {@link #createConversationSession(List, String)} from the given session
	 * ID and the input text.
	 * 
	 * @param aSessionId The session ID representing the current course of the
	 *        conversation.
	 * @param aRequest The request containing text for continuing the
	 *        conversation.
	 * 
	 * @return The according {@link ServerSessionElizaResponse} containing the
	 *         answer and the session ID (representing the course of the
	 *         conversation).
	 * 
	 * @throws IllegalArgumentException in case the ID is unknown for an
	 *         existing session.
	 */
	default ServerSessionElizaResponse continueConversationForSession( String aSessionId, ElizaRequest aRequest ) {
		return continueConversationForSession( aSessionId, aRequest.getMessage() );
	}

	/**
	 * Continues a conversation initiated by
	 * {@link #createConversationSession(List, String)} from the given session
	 * ID and the input text.
	 * 
	 * @param aSessionId The session ID representing the current course of the
	 *        conversation.
	 * @param aMessage The text for continuing the conversation.
	 * 
	 * @return The according {@link ServerSessionElizaResponse} containing the
	 *         answer and the session ID (representing the course of the
	 *         conversation).
	 * 
	 * @throws IllegalArgumentException in case the ID is unknown for an
	 *         existing session.
	 */
	ServerSessionElizaResponse continueConversationForSession( String aSessionId, String aMessage );

	/**
	 * Determines whether there is such a conversation already.
	 * 
	 * @param aSessionId The session ID for which to test if there is a
	 *        conversion.
	 * 
	 * @return True in case we already have such a conversation.
	 */
	boolean hasConversationSession( String aSessionId );

	/**
	 * Create a new {@link ServerSessionElizaResponse} from the given input text
	 * and supported {@link Locale} instances and return the according
	 * {@link ServerSessionElizaResponse}. Useful for clients making use of
	 * server-side state.
	 * 
	 * @param aLocales The {@link Locale} supported by the client.
	 * @param aRequest The greeting for initializing the
	 *        {@link ServerSessionElizaResponse}.
	 * 
	 * @return The according {@link ServerSessionElizaResponse} containing the
	 *         answer and the session ID (representing the course of the
	 *         conversation).
	 * 
	 * @throws IllegalArgumentException in case the ID is unknown for an
	 *         existing session.
	 */
	default ServerSessionElizaResponse createConversationSession( List<Locale> aLocales, ElizaRequest aRequest ) {
		return createConversationSession( aLocales, aRequest.getMessage() );
	}

	/**
	 * Create a new {@link ServerSessionElizaResponse} from the given input text
	 * and supported {@link Locale} instances and return the according
	 * {@link ServerSessionElizaResponse}. Useful for clients making use of
	 * server-side state.
	 * 
	 * @param aLocales The {@link Locale} supported by the client.
	 * @param aMessage The greeting for initializing the
	 *        {@link ServerSessionElizaResponse}.
	 * 
	 * @return The according {@link ServerSessionElizaResponse} containing the
	 *         answer and the session ID (representing the course of the
	 *         conversation).
	 * 
	 * @throws IllegalArgumentException in case the ID is unknown for an
	 *         existing session.
	 */
	ServerSessionElizaResponse createConversationSession( List<Locale> aLocales, String aMessage );

	/**
	 * Create a new {@link ServerSessionElizaResponse} from the given input text
	 * and supported {@link Locale} instances and return the according
	 * {@link ServerSessionElizaResponse}. Useful for clients making use of
	 * server-side state.
	 * 
	 * @param aLocales The {@link Locale} supported by the client.
	 * @param aRequest The greeting for initializing the
	 *        {@link ServerSessionElizaResponse} and the session ID for which to
	 *        create a conversation. conversion..
	 * 
	 * @return The according {@link ServerSessionElizaResponse} containing the
	 *         answer and the session ID (representing the course of the
	 *         conversation).
	 * 
	 * @throws IllegalArgumentException in case the ID is already in use for an
	 *         existing session.
	 */
	default ServerSessionElizaResponse createConversationSession( List<Locale> aLocales, ServerSessionElizaRequest aRequest ) {
		return createConversationSession( aLocales, aRequest.getSessionId(), aRequest.getMessage() );
	}

	/**
	 * Create a new {@link ServerSessionElizaResponse} from the given input text
	 * and supported {@link Locale} instances and return the according
	 * {@link ServerSessionElizaResponse}. Useful for clients making use of
	 * server-side state.
	 * 
	 * @param aLocales The {@link Locale} supported by the client.
	 * @param aSessionId The session ID for which to create a conversation.
	 *        conversion.
	 * @param aRequest The greeting for initializing the
	 *        {@link ServerSessionElizaResponse}.
	 * 
	 * @return The according {@link ServerSessionElizaResponse} containing the
	 *         answer and the session ID (representing the course of the
	 *         conversation).
	 * 
	 * @throws IllegalArgumentException in case the ID is already in use for an
	 *         existing session.
	 */
	default ServerSessionElizaResponse createConversationSession( List<Locale> aLocales, String aSessionId, ElizaRequest aRequest ) {
		return createConversationSession( aLocales, aSessionId, aRequest.getMessage() );
	}

	/**
	 * Create a new {@link ServerSessionElizaResponse} from the given input text
	 * and supported {@link Locale} instances and return the according
	 * {@link ServerSessionElizaResponse}. Useful for clients making use of
	 * server-side state.
	 * 
	 * @param aLocales The {@link Locale} supported by the client.
	 * @param aSessionId The session ID for which to create a conversation.
	 *        conversion.
	 * @param aMessage The greeting for initializing the
	 *        {@link ServerSessionElizaResponse}.
	 *
	 * @return The according {@link ServerSessionElizaResponse} containing the
	 *         answer and the session ID (representing the course of the
	 *         conversation).
	 * 
	 * @throws IllegalArgumentException in case the ID is already in use for an
	 *         existing session.
	 */
	ServerSessionElizaResponse createConversationSession( List<Locale> aLocales, String aSessionId, String aMessage );

}
