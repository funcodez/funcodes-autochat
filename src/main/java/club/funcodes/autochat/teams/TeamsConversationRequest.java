package club.funcodes.autochat.teams;

import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * POJO for an MS-Teams HTTP-Request.
 * 
 * See "https://json2csharp.com/json-to-pojo"
 */
public class TeamsConversationRequest {

	private ChannelData channelData = new ChannelData();
	private From from = new From();
	private Conversation conversation = new Conversation();

	private List<Attachment> attachments;
	private List<String> membersAdded;
	private List<String> membersRemoved;
	private List<Entity> entities;

	private Date localTimestamp;
	private Date timestamp;

	private String action;
	private String attachmentLayout;
	private String channelId;
	private String code;
	private String historyDisclosed;
	private String id;
	private String inputHint;
	private String localTimezone;
	private String locale;
	private String name;
	private String recipient;
	private String relatesTo;
	private String serviceUrl;
	private String speak;
	private String suggestedActions;
	private String summary;
	private String text;
	private String textFormat;
	private String topicName;
	private String type;
	private String value;
	private String replyToId;

	/**
	 * Instantiates a new teams request.
	 */
	public TeamsConversationRequest() {}

	/**
	 * Creates a Teams conversation request with the minimum required data.
	 * Useful when crating such an instance in unit tests.
	 * 
	 * @param aText The message from the Teams request.
	 * @param aConversationId The ID of the Teams conversation.
	 * @param aLocale The language setting.
	 */
	public TeamsConversationRequest( String aText, String aConversationId, Locale aLocale ) {
		text = aText;
		conversation.setId( aConversationId );
		locale = aLocale.getLanguage();
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param aAction the action to set
	 */
	public void setAction( String aAction ) {
		action = aAction;
	}

	/**
	 * Gets the attachment layout.
	 *
	 * @return the attachmentLayout
	 */
	public String getAttachmentLayout() {
		return attachmentLayout;
	}

	/**
	 * Sets the attachment layout.
	 *
	 * @param aAttachmentLayout the attachmentLayout to set
	 */
	public void setAttachmentLayout( String aAttachmentLayout ) {
		attachmentLayout = aAttachmentLayout;
	}

	/**
	 * Gets the attachments.
	 *
	 * @return the attachments
	 */
	public List<Attachment> getAttachments() {
		return attachments;
	}

	/**
	 * Sets the attachments.
	 *
	 * @param aAttachments the attachments to set
	 */
	public void setAttachments( List<Attachment> aAttachments ) {
		attachments = aAttachments;
	}

	/**
	 * Gets the channel data.
	 *
	 * @return the channelData
	 */
	public ChannelData getChannelData() {
		return channelData;
	}

	/**
	 * Sets the channel data.
	 *
	 * @param aChannelData the channelData to set
	 */
	public void setChannelData( ChannelData aChannelData ) {
		channelData = aChannelData;
	}

	/**
	 * Gets the channel id.
	 *
	 * @return the channelId
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * Sets the channel id.
	 *
	 * @param aChannelId the channelId to set
	 */
	public void setChannelId( String aChannelId ) {
		channelId = aChannelId;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param aCode the code to set
	 */
	public void setCode( String aCode ) {
		code = aCode;
	}

	/**
	 * Gets the conversation.
	 *
	 * @return the conversation
	 */
	public Conversation getConversation() {
		return conversation;
	}

	/**
	 * Sets the conversation.
	 *
	 * @param aConversation the conversation to set
	 */
	public void setConversation( Conversation aConversation ) {
		conversation = aConversation;
	}

	/**
	 * Gets the entities.
	 *
	 * @return the entities
	 */
	public List<Entity> getEntities() {
		return entities;
	}

	/**
	 * Sets the entities.
	 *
	 * @param aEntities the entities to set
	 */
	public void setEntities( List<Entity> aEntities ) {
		entities = aEntities;
	}

	/**
	 * Gets the from.
	 *
	 * @return the from
	 */
	public From getFrom() {
		return from;
	}

	/**
	 * Sets the from.
	 *
	 * @param aFrom the from to set
	 */
	public void setFrom( From aFrom ) {
		from = aFrom;
	}

	/**
	 * Gets the history disclosed.
	 *
	 * @return the historyDisclosed
	 */
	public String getHistoryDisclosed() {
		return historyDisclosed;
	}

	/**
	 * Sets the history disclosed.
	 *
	 * @param aHistoryDisclosed the historyDisclosed to set
	 */
	public void setHistoryDisclosed( String aHistoryDisclosed ) {
		historyDisclosed = aHistoryDisclosed;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param aId the id to set
	 */
	public void setId( String aId ) {
		id = aId;
	}

	/**
	 * Gets the input hint.
	 *
	 * @return the inputHint
	 */
	public String getInputHint() {
		return inputHint;
	}

	/**
	 * Sets the input hint.
	 *
	 * @param aInputHint the inputHint to set
	 */
	public void setInputHint( String aInputHint ) {
		inputHint = aInputHint;
	}

	/**
	 * Gets the local timestamp.
	 *
	 * @return the localTimestamp
	 */
	public Date getLocalTimestamp() {
		return localTimestamp;
	}

	/**
	 * Sets the local timestamp.
	 *
	 * @param aLocalTimestamp the localTimestamp to set
	 */
	public void setLocalTimestamp( Date aLocalTimestamp ) {
		localTimestamp = aLocalTimestamp;
	}

	/**
	 * Gets the local timezone.
	 *
	 * @return the localTimezone
	 */
	public String getLocalTimezone() {
		return localTimezone;
	}

	/**
	 * Sets the local timezone.
	 *
	 * @param aLocalTimezone the localTimezone to set
	 */
	public void setLocalTimezone( String aLocalTimezone ) {
		localTimezone = aLocalTimezone;
	}

	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * Sets the locale.
	 *
	 * @param aLocale the locale to set
	 */
	public void setLocale( String aLocale ) {
		locale = aLocale;
	}

	/**
	 * Gets the members added.
	 *
	 * @return the membersAdded
	 */
	public List<String> getMembersAdded() {
		return membersAdded;
	}

	/**
	 * Sets the members added.
	 *
	 * @param aMembersAdded the membersAdded to set
	 */
	public void setMembersAdded( List<String> aMembersAdded ) {
		membersAdded = aMembersAdded;
	}

	/**
	 * Gets the members removed.
	 *
	 * @return the membersRemoved
	 */
	public List<String> getMembersRemoved() {
		return membersRemoved;
	}

	/**
	 * Sets the members removed.
	 *
	 * @param aMembersRemoved the membersRemoved to set
	 */
	public void setMembersRemoved( List<String> aMembersRemoved ) {
		membersRemoved = aMembersRemoved;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param aName the name to set
	 */
	public void setName( String aName ) {
		name = aName;
	}

	/**
	 * Gets the recipient.
	 *
	 * @return the recipient
	 */
	public String getRecipient() {
		return recipient;
	}

	/**
	 * Sets the recipient.
	 *
	 * @param aRecipient the recipient to set
	 */
	public void setRecipient( String aRecipient ) {
		recipient = aRecipient;
	}

	/**
	 * Gets the relates to.
	 *
	 * @return the relatesTo
	 */
	public String getRelatesTo() {
		return relatesTo;
	}

	/**
	 * Sets the relates to.
	 *
	 * @param aRelatesTo the relatesTo to set
	 */
	public void setRelatesTo( String aRelatesTo ) {
		relatesTo = aRelatesTo;
	}

	/**
	 * Gets the reply to id.
	 *
	 * @return the replyToId
	 */
	public String getReplyToId() {
		return replyToId;
	}

	/**
	 * Sets the reply to id.
	 *
	 * @param aReplyToId the replyToId to set
	 */
	public void setReplyToId( String aReplyToId ) {
		replyToId = aReplyToId;
	}

	/**
	 * Gets the service url.
	 *
	 * @return the serviceUrl
	 */
	public String getServiceUrl() {
		return serviceUrl;
	}

	/**
	 * Sets the service url.
	 *
	 * @param aServiceUrl the serviceUrl to set
	 */
	public void setServiceUrl( String aServiceUrl ) {
		serviceUrl = aServiceUrl;
	}

	/**
	 * Gets the speak.
	 *
	 * @return the speak
	 */
	public String getSpeak() {
		return speak;
	}

	/**
	 * Sets the speak.
	 *
	 * @param aSpeak the speak to set
	 */
	public void setSpeak( String aSpeak ) {
		speak = aSpeak;
	}

	/**
	 * Gets the suggested actions.
	 *
	 * @return the suggestedActions
	 */
	public String getSuggestedActions() {
		return suggestedActions;
	}

	/**
	 * Sets the suggested actions.
	 *
	 * @param aSuggestedActions the suggestedActions to set
	 */
	public void setSuggestedActions( String aSuggestedActions ) {
		suggestedActions = aSuggestedActions;
	}

	/**
	 * Gets the summary.
	 *
	 * @return the summary
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * Sets the summary.
	 *
	 * @param aSummary the summary to set
	 */
	public void setSummary( String aSummary ) {
		summary = aSummary;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Extracts the "@Somebody" from the {@link #getText()}, actually denoted as
	 * "<at>Eliza</at>".
	 * 
	 * @return The pure message without the sender's name of the message.
	 */
	public String toMessage() {
		if ( text == null ) {
			return null;
		}
		return text.replaceAll( "(?s)<at>.*?</at>", "" ).trim(); // Remove "<at>Eliza</at>" and whitespace characters
	}

	/**
	 * Converts the {@link String} returned by the {@link #getLocale()} method
	 * into a {@link Locale} instance (as good as it gets).
	 * 
	 * @return The corresponding {@link Locale} instance.
	 */
	public Locale toLocale() {
		return Locale.forLanguageTag( locale );
	}

	/**
	 * Sets the text.
	 *
	 * @param aText the text to set
	 */
	public void setText( String aText ) {
		text = aText;
	}

	/**
	 * Gets the text format.
	 *
	 * @return the textFormat
	 */
	public String getTextFormat() {
		return textFormat;
	}

	/**
	 * Sets the text format.
	 *
	 * @param aTextFormat the textFormat to set
	 */
	public void setTextFormat( String aTextFormat ) {
		textFormat = aTextFormat;
	}

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the timestamp.
	 *
	 * @param aTimestamp the timestamp to set
	 */
	public void setTimestamp( Date aTimestamp ) {
		timestamp = aTimestamp;
	}

	/**
	 * Gets the topic name.
	 *
	 * @return the topicName
	 */
	public String getTopicName() {
		return topicName;
	}

	/**
	 * Sets the topic name.
	 *
	 * @param aTopicName the topicName to set
	 */
	public void setTopicName( String aTopicName ) {
		topicName = aTopicName;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param aType the type to set
	 */
	public void setType( String aType ) {
		type = aType;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param aValue the value to set
	 */
	public void setValue( String aValue ) {
		value = aValue;
	}

	/**
	 * The Class Attachment.
	 */
	public static class Attachment {

		private String content;
		private String contentType;
		private String contentUrl;
		private String name;
		private String thumbnailUrl;

		/**
		 * Instantiates a new attachment.
		 */
		public Attachment() {}

		/**
		 * Gets the content.
		 *
		 * @return the content
		 */
		public String getContent() {
			return content;
		}

		/**
		 * Sets the content.
		 *
		 * @param aContent the content to set
		 */
		public void setContent( String aContent ) {
			content = aContent;
		}

		/**
		 * Gets the content type.
		 *
		 * @return the contentType
		 */
		public String getContentType() {
			return contentType;
		}

		/**
		 * Sets the content type.
		 *
		 * @param aContentType the contentType to set
		 */
		public void setContentType( String aContentType ) {
			contentType = aContentType;
		}

		/**
		 * Gets the content url.
		 *
		 * @return the contentUrl
		 */
		public String getContentUrl() {
			return contentUrl;
		}

		/**
		 * Sets the content url.
		 *
		 * @param aContentUrl the contentUrl to set
		 */
		public void setContentUrl( String aContentUrl ) {
			contentUrl = aContentUrl;
		}

		/**
		 * Gets the name.
		 *
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Sets the name.
		 *
		 * @param aName the name to set
		 */
		public void setName( String aName ) {
			name = aName;
		}

		/**
		 * Gets the thumbnail url.
		 *
		 * @return the thumbnailUrl
		 */
		public String getThumbnailUrl() {
			return thumbnailUrl;
		}

		/**
		 * Sets the thumbnail url.
		 *
		 * @param aThumbnailUrl the thumbnailUrl to set
		 */
		public void setThumbnailUrl( String aThumbnailUrl ) {
			thumbnailUrl = aThumbnailUrl;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "Attachment [content=" + content + ", contentType=" + contentType + ", contentUrl=" + contentUrl + ", name=" + name + ", thumbnailUrl=" + thumbnailUrl + "]";
		}
	}

	/**
	 * The Class Channel.
	 */
	public static class Channel {

		/**
		 * Instantiates a new channel.
		 */
		public Channel() {}

		private String id;

		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public String getId() {
			return id;
		}

		/**
		 * Sets the id.
		 *
		 * @param aId the id to set
		 */
		public void setId( String aId ) {
			id = aId;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "Channel [id=" + id + "]";
		}
	}

	/**
	 * The Class Team.
	 */
	public static class Team {

		/**
		 * Instantiates a new team.
		 */
		public Team() {}

		private String id;

		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public String getId() {
			return id;
		}

		/**
		 * Sets the id.
		 *
		 * @param aId the id to set
		 */
		public void setId( String aId ) {
			id = aId;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "Team [id=" + id + "]";
		}
	}

	/**
	 * The Class Tenant.
	 */
	public static class Tenant {

		/**
		 * Instantiates a new tenant.
		 */
		public Tenant() {}

		private String id;

		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public String getId() {
			return id;
		}

		/**
		 * Sets the id.
		 *
		 * @param aId the id to set
		 */
		public void setId( String aId ) {
			id = aId;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "Tenant [id=" + id + "]";
		}
	}

	/**
	 * The Class ChannelData.
	 */
	public static class ChannelData {

		private Channel channel;
		private Team team;
		private String teamsChannelId;
		private String teamsTeamId;
		private Tenant tenant;

		/**
		 * Instantiates a new channel data.
		 */
		public ChannelData() {}

		/**
		 * Gets the channel.
		 *
		 * @return the channel
		 */
		public Channel getChannel() {
			return channel;
		}

		/**
		 * Sets the channel.
		 *
		 * @param aChannel the channel to set
		 */
		public void setChannel( Channel aChannel ) {
			channel = aChannel;
		}

		/**
		 * Gets the team.
		 *
		 * @return the team
		 */
		public Team getTeam() {
			return team;
		}

		/**
		 * Sets the team.
		 *
		 * @param aTeam the team to set
		 */
		public void setTeam( Team aTeam ) {
			team = aTeam;
		}

		/**
		 * Gets the teams channel id.
		 *
		 * @return the teamsChannelId
		 */
		public String getTeamsChannelId() {
			return teamsChannelId;
		}

		/**
		 * Sets the teams channel id.
		 *
		 * @param aTeamsChannelId the teamsChannelId to set
		 */
		public void setTeamsChannelId( String aTeamsChannelId ) {
			teamsChannelId = aTeamsChannelId;
		}

		/**
		 * Gets the teams team id.
		 *
		 * @return the teamsTeamId
		 */
		public String getTeamsTeamId() {
			return teamsTeamId;
		}

		/**
		 * Sets the teams team id.
		 *
		 * @param aTeamsTeamId the teamsTeamId to set
		 */
		public void setTeamsTeamId( String aTeamsTeamId ) {
			teamsTeamId = aTeamsTeamId;
		}

		/**
		 * Gets the tenant.
		 *
		 * @return the tenant
		 */
		public Tenant getTenant() {
			return tenant;
		}

		/**
		 * Sets the tenant.
		 *
		 * @param aTenant the tenant to set
		 */
		public void setTenant( Tenant aTenant ) {
			tenant = aTenant;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "ChannelData [channel=" + channel + ", team=" + team + ", teamsChannelId=" + teamsChannelId + ", teamsTeamId=" + teamsTeamId + ", tenant=" + tenant + "]";
		}
	}

	/**
	 * The Class Conversation.
	 */
	public static class Conversation {

		private String conversationType;
		private String id;
		private boolean group;
		private String name;
		private String tenantId;

		/**
		 * Instantiates a new conversation.
		 */
		public Conversation() {}

		/**
		 * Gets the conversation type.
		 *
		 * @return the conversationType
		 */
		public String getConversationType() {
			return conversationType;
		}

		/**
		 * Sets the conversation type.
		 *
		 * @param aConversationType the conversationType to set
		 */
		public void setConversationType( String aConversationType ) {
			conversationType = aConversationType;
		}

		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public String getId() {
			return id;
		}

		/**
		 * Sets the id.
		 *
		 * @param aId the id to set
		 */
		public void setId( String aId ) {
			id = aId;
		}

		/**
		 * Checks if is group.
		 *
		 * @return the group
		 */
		public boolean isGroup() {
			return group;
		}

		/**
		 * Sets the group.
		 *
		 * @param aIsGroup the group to set
		 */
		public void setGroup( boolean aIsGroup ) {
			group = aIsGroup;
		}

		/**
		 * Gets the name.
		 *
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Sets the name.
		 *
		 * @param aName the name to set
		 */
		public void setName( String aName ) {
			name = aName;
		}

		/**
		 * Gets the tenant id.
		 *
		 * @return the tenantId
		 */
		public String getTenantId() {
			return tenantId;
		}

		/**
		 * Sets the tenant id.
		 *
		 * @param aTenantId the tenantId to set
		 */
		public void setTenantId( String aTenantId ) {
			tenantId = aTenantId;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "Conversation [conversationType=" + conversationType + ", id=" + id + ", group=" + group + ", name=" + name + ", tenantId=" + tenantId + "]";
		}
	}

	/**
	 * The Class Entity.
	 */
	public static class Entity {

		private String country;
		private String locale;
		private String platform;
		private String timezone;
		private String type;

		/**
		 * Instantiates a new entity.
		 */
		public Entity() {}

		/**
		 * Gets the country.
		 *
		 * @return the country
		 */
		public String getCountry() {
			return country;
		}

		/**
		 * Sets the country.
		 *
		 * @param aCountry the country to set
		 */
		public void setCountry( String aCountry ) {
			country = aCountry;
		}

		/**
		 * Gets the locale.
		 *
		 * @return the locale
		 */
		public String getLocale() {
			return locale;
		}

		/**
		 * Sets the locale.
		 *
		 * @param aLocale the locale to set
		 */
		public void setLocale( String aLocale ) {
			locale = aLocale;
		}

		/**
		 * Gets the platform.
		 *
		 * @return the platform
		 */
		public String getPlatform() {
			return platform;
		}

		/**
		 * Sets the platform.
		 *
		 * @param aPlatform the platform to set
		 */
		public void setPlatform( String aPlatform ) {
			platform = aPlatform;
		}

		/**
		 * Gets the timezone.
		 *
		 * @return the timezone
		 */
		public String getTimezone() {
			return timezone;
		}

		/**
		 * Sets the timezone.
		 *
		 * @param aTimezone the timezone to set
		 */
		public void setTimezone( String aTimezone ) {
			timezone = aTimezone;
		}

		/**
		 * Gets the type.
		 *
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * Sets the type.
		 *
		 * @param aType the type to set
		 */
		public void setType( String aType ) {
			type = aType;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "Entity [country=" + country + ", locale=" + locale + ", platform=" + platform + ", timezone=" + timezone + ", type=" + type + "]";
		}
	}

	/**
	 * The Class From.
	 */
	public static class From {

		private String aadObjectId;
		private String id;
		private String name;

		/**
		 * Instantiates a new from.
		 */
		public From() {}

		/**
		 * Gets the aad object id.
		 *
		 * @return the aadObjectId
		 */
		public String getAadObjectId() {
			return aadObjectId;
		}

		/**
		 * Sets the aad object id.
		 *
		 * @param aAadObjectId the aadObjectId to set
		 */
		public void setAadObjectId( String aAadObjectId ) {
			aadObjectId = aAadObjectId;
		}

		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public String getId() {
			return id;
		}

		/**
		 * Sets the id.
		 *
		 * @param aId the id to set
		 */
		public void setId( String aId ) {
			id = aId;
		}

		/**
		 * Gets the name.
		 *
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Sets the name.
		 *
		 * @param aName the name to set
		 */
		public void setName( String aName ) {
			name = aName;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "From [aadObjectId=" + aadObjectId + ", id=" + id + ", name=" + name + "]";
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "TeamsConversationRequest [action=" + action + ", attachmentLayout=" + attachmentLayout + ", attachments=" + attachments + ", channelData=" + channelData + ", channelId=" + channelId + ", code=" + code + ", conversation=" + conversation + ", entities=" + entities + ", from=" + from + ", historyDisclosed=" + historyDisclosed + ", id=" + id + ", inputHint=" + inputHint + ", localTimestamp=" + localTimestamp + ", localTimezone=" + localTimezone + ", locale=" + locale + ", membersAdded=" + membersAdded + ", membersRemoved=" + membersRemoved + ", name=" + name + ", recipient=" + recipient + ", relatesTo=" + relatesTo + ", serviceUrl=" + serviceUrl + ", speak=" + speak + ", suggestedActions=" + suggestedActions + ", summary=" + summary + ", text=" + text + ", textFormat=" + textFormat + ", timestamp=" + timestamp + ", topicName=" + topicName + ", type=" + type + ", value=" + value + ", replyToId=" + replyToId + "]";
	}
}
