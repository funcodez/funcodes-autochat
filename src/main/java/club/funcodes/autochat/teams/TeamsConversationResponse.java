package club.funcodes.autochat.teams;

/**
 * POJO for an MS-Teams HTTP-Response.
 * 
 * See "https://json2csharp.com/json-to-pojo"
 */
public class TeamsConversationResponse {

	private static final String TYPE_MESSAGE = "message";

	private String type;
	private String text;

	/**
	 * Instantiates a new teams response.
	 *
	 * @param aType the type
	 * @param aText the text
	 */
	public TeamsConversationResponse( String aType, String aText ) {
		type = aType;
		text = aText;
	}

	/**
	 * Instantiates a new teams response.
	 *
	 * @param aText the text
	 */
	public TeamsConversationResponse( String aText ) {
		type = TYPE_MESSAGE;
		text = aText;
	}

	/**
	 * Instantiates a new teams response.
	 */
	public TeamsConversationResponse() {}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param aType the type to set
	 */
	public void setType( String aType ) {
		type = aType;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 *
	 * @param aText the text to set
	 */
	public void setText( String aText ) {
		text = aText;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "TeamsConversationResponse [type=" + type + ", text=" + text + "]";
	}
}
