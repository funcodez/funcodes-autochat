module club.funcodes.autochat {
	requires org.refcodes.data;
	requires org.refcodes.logger;
	requires org.refcodes.archetype;
	requires org.refcodes.rest;

	opens club.funcodes.autochat.service;

	exports club.funcodes.autochat.service;
	exports club.funcodes.autochat.teams;
}
