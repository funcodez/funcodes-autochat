package club.funcodes.autochat;

import java.net.MalformedURLException;
import java.util.Locale;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.Trap;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.RestRequestBuilder;
import org.refcodes.rest.RestResponse;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.MediaType;
import club.funcodes.autochat.service.ClientStateElizaRequest;
import club.funcodes.autochat.service.ClientStateElizaResponse;
import club.funcodes.autochat.service.ElizaRequest;
import club.funcodes.autochat.service.ElizaState;

public class ClientStateElizaServerTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	private RestfulHttpClient theRestClient = new HttpRestClient();
	private static int _port;
	private static Main _server;

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@BeforeAll
	public static void startTheServer() throws Exception {
		_port = PortManagerSingleton.getInstance().bindAnyPort();
		_server = Main.createServer( _port, true );
		_server.startServer();
	}

	@AfterAll
	public static void stopTheServer() throws Exception {
		_server.stopServer();
	}

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testElizaDe() throws MalformedURLException, HttpStatusException {
		ElizaState theState = say( "Hallo!", Locale.GERMAN );
		theState = say( "Wer bin ich?", theState );
		theState = say( "Woher komme ich?", theState );
		theState = say( "Wohin werde ich gehen?", theState );
		theState = say( "Wer ist Eliza?", theState );
		theState = say( "Nochmal, wer ist Eliza?", theState );
	}

	@Test
	public void testElizaEn() throws MalformedURLException, HttpStatusException {
		ElizaState theState = say( "Hello!", Locale.ENGLISH );
		theState = say( "Who am I?", theState );
		theState = say( "Where do I come from?", theState );
		theState = say( "Where will I go?", theState );
		theState = say( "Who is Eliza?", theState );
		theState = say( "Again, who is Eliza?", theState );
	}

	// -------------------------------------------------------------------------
	/**
	 * Open the conversation (PUT):
	 */
	private ElizaState say( String aMessage, Locale aLocale ) throws MalformedURLException, HttpStatusException {
		System.out.println( "Ich: " + aMessage );
		RestRequestBuilder theRequest = theRestClient.buildPut( "http://localhost:" + _port + "/autochat", new ElizaRequest( aMessage ) );
		theRequest.getHeaderFields().putAcceptLanguages( aLocale );
		theRequest.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_JSON );
		theRequest.getHeaderFields().putContentType( MediaType.APPLICATION_JSON );
		RestResponse theRestResponse = theRequest.toRestResponse();
		System.out.print( "Eliza [" + theRestResponse.getHttpStatusCode() + "]: " );
		if ( theRestResponse.getHttpStatusCode().isErrorStatus() ) {
			System.out.println( theRestResponse.getHttpBody() );
			throw theRestResponse.getHttpStatusCode().toHttpStatusException( theRestResponse.getHttpBody() );
		}
		else {
			try {
				ClientStateElizaResponse theResponse = theRestResponse.getResponse( ClientStateElizaResponse.class );
				System.out.println( theResponse.getAnswer() );
				return theResponse.getState();
			}
			catch ( Exception e ) {
				System.out.println( Trap.asMessage( e ) + ": " + theRestResponse.getHttpBody() );
				throw new RuntimeException( "Cannot get response for message \"" + aMessage + "\" for locale <" + aLocale + ">!", e );
			}
		}
	}

	private ElizaState say( String aMessage, ElizaState aState ) throws MalformedURLException, HttpStatusException {
		System.out.println( "Ich: " + aMessage );
		RestRequestBuilder theRequest = theRestClient.buildPost( "http://localhost:" + _port + "/autochat", new ClientStateElizaRequest( aMessage, aState ) );
		theRequest.getHeaderFields().putAcceptLanguages( Locale.GERMAN );
		theRequest.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_JSON );
		theRequest.getHeaderFields().putContentType( MediaType.APPLICATION_JSON );
		RestResponse theRestResponse = theRequest.toRestResponse();
		System.out.print( "Eliza [" + theRestResponse.getHttpStatusCode() + "]: " );
		if ( theRestResponse.getHttpStatusCode().isErrorStatus() ) {
			System.out.println( theRestResponse.getHttpBody() );
			throw theRestResponse.getHttpStatusCode().toHttpStatusException( theRestResponse.getHttpBody() );
		}
		else {
			ClientStateElizaResponse theResponse = theRestResponse.getResponse( ClientStateElizaResponse.class );
			System.out.println( theResponse.getAnswer() );
			return theResponse.getState();
		}
	}
}