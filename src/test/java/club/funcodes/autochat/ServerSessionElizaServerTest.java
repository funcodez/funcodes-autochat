package club.funcodes.autochat;

import java.net.MalformedURLException;
import java.util.Locale;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.RestRequestBuilder;
import org.refcodes.rest.RestResponse;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.MediaType;
import club.funcodes.autochat.service.ElizaRequest;
import club.funcodes.autochat.service.ServerSessionElizaResponse;

/**
 * Tests the server side state HTTP interface.
 */
public class ServerSessionElizaServerTest {

	private RestfulHttpClient theRestClient = new HttpRestClient();
	private static int _port;
	private static Main _server;

	@BeforeAll
	public static void startTheServer() throws Exception {
		_port = PortManagerSingleton.getInstance().bindAnyPort();
		_server = Main.createServer( _port, true );
		_server.startServer();
	}

	@AfterAll
	public static void stopTheServer() throws Exception {
		_server.stopServer();
	}

	// -------------------------------------------------------------------------

	@Test
	public void testElizaDe() throws MalformedURLException, HttpStatusException {
		ServerSessionElizaResponse theSession = say( "Hallo!", Locale.GERMAN );
		theSession = say( "Wer bin ich?", theSession );
		theSession = say( "Woher komme ich?", theSession );
		theSession = say( "Wohin werde ich gehen?", theSession );
		theSession = say( "Wer ist Eliza?", theSession );
		theSession = say( "Nochmal, wer ist Eliza?", theSession );
	}

	@Test
	public void testElizaEn() throws MalformedURLException, HttpStatusException {
		ServerSessionElizaResponse theSession = say( "Hello!", Locale.ENGLISH );
		theSession = say( "Who am I?", theSession );
		theSession = say( "Where do I come from?", theSession );
		theSession = say( "Where will I go?", theSession );
		theSession = say( "Who is Eliza?", theSession );
		theSession = say( "Again, who is Eliza?", theSession );
	}

	// -------------------------------------------------------------------------

	/**
	 * Open the conversation (PUT):
	 */
	private ServerSessionElizaResponse say( String aMessage, Locale aLocale ) throws MalformedURLException, HttpStatusException {
		System.out.println( "Ich: " + aMessage );
		RestRequestBuilder theRequest = theRestClient.buildPut( "http://localhost:" + _port + "/autochat/sessions", new ElizaRequest( aMessage ) );
		theRequest.getHeaderFields().putAcceptLanguages( aLocale );
		theRequest.getHeaderFields().putContentType( MediaType.APPLICATION_JSON );
		theRequest.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_JSON );
		RestResponse theRestResponse = theRequest.toRestResponse();
		System.out.print( "Eliza [" + theRestResponse.getHttpStatusCode() + "]: " );
		if ( theRestResponse.getHttpStatusCode().isErrorStatus() ) {
			System.out.println( theRestResponse.getHttpBody() );
			throw theRestResponse.getHttpStatusCode().toHttpStatusException( theRestResponse.getHttpBody() );
		}
		else {
			ServerSessionElizaResponse theSession = theRestResponse.getResponse( ServerSessionElizaResponse.class );
			System.out.println( theSession.getAnswer() );
			return theSession;
		}
	}

	/**
	 * Continue the conversation (POST):
	 */
	private ServerSessionElizaResponse say( String aMessage, ServerSessionElizaResponse aSession ) throws MalformedURLException, HttpStatusException {
		System.out.println( "Ich: " + aMessage );
		RestRequestBuilder theRequest = theRestClient.buildPost( "http://localhost:" + _port + "/autochat/sessions/" + aSession.getSessionId(), new ElizaRequest( aMessage ) );
		theRequest.getHeaderFields().putAcceptLanguages( Locale.GERMAN );
		theRequest.getHeaderFields().putContentType( MediaType.APPLICATION_JSON );
		theRequest.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_JSON );
		RestResponse theRestResponse = theRequest.toRestResponse();
		System.out.print( "Eliza [" + theRestResponse.getHttpStatusCode() + "]: " );
		if ( theRestResponse.getHttpStatusCode().isErrorStatus() ) {
			System.out.println( theRestResponse.getHttpBody() );
			throw theRestResponse.getHttpStatusCode().toHttpStatusException( theRestResponse.getHttpBody() );
		}
		else {
			ServerSessionElizaResponse theSession = theRestResponse.getResponse( ServerSessionElizaResponse.class );
			System.out.println( theSession.getAnswer() );
			return theSession;
		}
	}
}