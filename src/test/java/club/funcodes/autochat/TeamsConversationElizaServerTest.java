package club.funcodes.autochat;

import java.net.MalformedURLException;
import java.util.Locale;
import java.util.UUID;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.RestRequestBuilder;
import org.refcodes.rest.RestResponse;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.MediaType;
import club.funcodes.autochat.teams.TeamsConversationRequest;
import club.funcodes.autochat.teams.TeamsConversationResponse;

/**
 * Tests the server side state HTTP interface.
 */
public class TeamsConversationElizaServerTest {

	private RestfulHttpClient theRestClient = new HttpRestClient();
	private static int _port;
	private static Main _server;

	@BeforeAll
	public static void startTheServer() throws Exception {
		_port = PortManagerSingleton.getInstance().bindAnyPort();
		_server = Main.createServer( _port, true );
		_server.startServer();
	}

	@AfterAll
	public static void stopTheServer() throws Exception {
		_server.stopServer();
	}

	// -------------------------------------------------------------------------

	@Test
	public void testElizaDe() throws HttpStatusException, MalformedURLException {
		String theSessionId = UUID.randomUUID().toString();
		say( "Hallo!", theSessionId, Locale.GERMAN );
		say( "Wer bin ich?", theSessionId, Locale.GERMAN );
		say( "Woher komme ich?", theSessionId, Locale.GERMAN );
		say( "Wohin werde ich gehen?", theSessionId, Locale.GERMAN );
		say( "Wer ist Eliza?", theSessionId, Locale.GERMAN );
		say( "Nochmal, wer ist Eliza?", theSessionId, Locale.GERMAN );
	}

	@Test
	public void testElizaEn() throws MalformedURLException, HttpStatusException {
		String theSessionId = UUID.randomUUID().toString();
		say( "Hello!", theSessionId, Locale.ENGLISH );
		say( "Who am I?", theSessionId, Locale.ENGLISH );
		say( "Where do I come from?", theSessionId, Locale.ENGLISH );
		say( "Where will I go?", theSessionId, Locale.ENGLISH );
		say( "Who is Eliza?", theSessionId, Locale.ENGLISH );
		say( "Again, who is Eliza?", theSessionId, Locale.ENGLISH );
	}

	// -------------------------------------------------------------------------

	/**
	 * Open the conversation (PUT):
	 */
	private void say( String aMessage, String aSessionId, Locale aLocale ) throws MalformedURLException, HttpStatusException {
		System.out.println( "Ich: " + aMessage );
		TeamsConversationRequest theTeamsRequest = new TeamsConversationRequest( aMessage, aSessionId, aLocale );
		RestRequestBuilder theRequest = theRestClient.buildPost( "http://localhost:" + _port + "/autochat/teams", theTeamsRequest );
		theRequest.getHeaderFields().putAcceptLanguages( aLocale );
		theRequest.getHeaderFields().putContentType( MediaType.APPLICATION_JSON );
		theRequest.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_JSON );
		RestResponse theRestResponse = theRequest.toRestResponse();
		System.out.print( "Eliza [" + theRestResponse.getHttpStatusCode() + "]: " );
		if ( theRestResponse.getHttpStatusCode().isErrorStatus() ) {
			System.out.println( theRestResponse.getHttpBody() );
			throw theRestResponse.getHttpStatusCode().toHttpStatusException( theRestResponse.getHttpBody() );
		}
		else {
			TeamsConversationResponse theSession = theRestResponse.getResponse( TeamsConversationResponse.class );
			System.out.println( theSession.getText() );
		}
	}
}