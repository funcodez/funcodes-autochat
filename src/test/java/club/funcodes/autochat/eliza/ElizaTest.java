package club.funcodes.autochat.eliza;

import java.io.IOException;
import org.junit.jupiter.api.Test;

public class ElizaTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testElizaEn() throws IOException {
		Eliza eliza = new Eliza();
		String say = "Hello";
		String response = eliza.processInput( say );
		System.out.println( "Me: " + say + "\nEliza: " + response );
		say = "Who is Eliza?";
		response = eliza.processInput( say );
		System.out.println( "Me: " + say + "\nEliza: " + response );
		say = "Who exactly is Eliza?";
		response = eliza.processInput( say );
		System.out.println( "Me: " + say + "\nEliza: " + response );
		say = "Can you help me?";
		response = eliza.processInput( say );
		System.out.println( "Me: " + say + "\nEliza: " + response );
		say = "Bye";
		response = eliza.processInput( say );
		System.out.println( "Me: " + say + "\nEliza: " + response );
		System.out.println();
	}

	@Test
	public void testElizaDe() throws IOException {
		Eliza eliza = new Eliza( "eliza-de.script" );
		String say = "Hallo";
		String response = eliza.processInput( say );
		System.out.println( "Ich: " + say + "\nEliza: " + response );
		say = "Was ist Eliza?";
		response = eliza.processInput( say );
		System.out.println( "Ich: " + say + "\nEliza: " + response );
		say = "Was genau ist ELiza?";
		response = eliza.processInput( say );
		System.out.println( "Ich: " + say + "\nEliza: " + response );
		say = "Kannst du mir helfen?";
		response = eliza.processInput( say );
		System.out.println( "Ich: " + say + "\nEliza: " + response );
		say = "Ciao";
		response = eliza.processInput( say );
		System.out.println( "Ich: " + say + "\nEliza: " + response );
		System.out.println();
	}
}
