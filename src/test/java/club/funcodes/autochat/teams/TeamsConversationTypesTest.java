package club.funcodes.autochat.teams;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.Locale;
import org.junit.jupiter.api.Test;
import org.refcodes.properties.JsonPropertiesBuilder;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.PropertiesBuilderImpl;

public class TeamsConversationTypesTest {
	/**
	 * Tests marshaling and unmarshaling of the {@link TeamsConversationRequest}
	 * type (ehttps://www.metacodes.proly interesting the date conversion and
	 * the locale).
	 */
	@Test
	public void testTeamsConversationRequestType() throws IOException, ParseException {
		JsonPropertiesBuilder theJsonProperties = new JsonPropertiesBuilder( getClass(), "/SampleTeamsConversationRequest.json" );
		System.out.println( "JSON" );
		for ( String eKey : theJsonProperties.sortedKeys() ) {
			System.out.println( eKey + " = " + theJsonProperties.get( eKey ) );
		}
		TeamsConversationRequest theRequest1 = theJsonProperties.toType( TeamsConversationRequest.class );
		PropertiesBuilder theTypeProperties = new PropertiesBuilderImpl( theRequest1 );
		System.out.println( "\nTYPE" );
		for ( String eKey : theTypeProperties.sortedKeys() ) {
			System.out.println( eKey + " = " + theTypeProperties.get( eKey ) );
		}
		assertEquals( theJsonProperties.size(), theTypeProperties.size() );
		TeamsConversationRequest theRequest2 = theTypeProperties.toType( TeamsConversationRequest.class );
		assertEquals( theRequest1.getTimestamp(), theRequest2.getTimestamp() );
		assertEquals( theRequest1.getLocalTimestamp(), theRequest2.getLocalTimestamp() );
		System.out.println( "JSON properties locale = " + theJsonProperties.get( "locale" ) );
		System.out.println( "TYPE properties locale = " + theTypeProperties.get( "locale" ) );
		System.out.println( "Request 1 locale = " + theRequest1.toLocale() );
		System.out.println( "Request 2 locale = " + theRequest2.toLocale() );
		assertEquals( theRequest1.toLocale(), Locale.GERMANY );
		assertEquals( theRequest1.toLocale(), theRequest2.toLocale() );
	}
}
